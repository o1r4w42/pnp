package de.pnp.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import de.pnp.R;
import de.pnp.fragment.sub.PnPSubFragment;
import de.pnp.fragment.sub.character.PnPSubFragmentCharAttrMain;
import de.pnp.fragment.sub.character.PnPSubFragmentCharAttrSub;
import de.pnp.fragment.sub.character.PnPSubFragmentCharPersonal;
import de.pnp.fragment.sub.character.PnPSubFragmentCharSkillsActive;

public class PnPFragmentCharacter extends PnPFragment {

    public PnPFragmentCharacter() {
        super("Character");
        addFragment(new PnPSubFragmentCharPersonal());
        addFragment(new PnPSubFragmentCharAttrMain());
        addFragment(new PnPSubFragmentCharAttrSub());
        addFragment(new PnPSubFragmentCharSkillsActive());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_char,
                container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button personalButton = view.findViewById(R.id.button_fragment_char_personal);
        personalButton.setOnClickListener(e -> {
            enterFragment(R.id.frame_fragment_char_personal, PnPSubFragment.SUB_FRAG.CHAR_PERSONAL);
        });

        Button attrButton = view.findViewById(R.id.button_fragment_char_attr_main);
        attrButton.setOnClickListener(e -> {
            enterFragment(R.id.frame_fragment_char_attr_main,  PnPSubFragment.SUB_FRAG.CHAR_ATTR_MAIN);
        });

        Button attrSubButton = view.findViewById(R.id.button_fragment_char_attr_sub);
        attrSubButton.setOnClickListener(e -> {
            enterFragment(R.id.frame_fragment_char_attr_sub,  PnPSubFragment.SUB_FRAG.CHAR_ATTR_SUB);
        });

        Button skillsActButton = view.findViewById(R.id.button_fragment_char_skills_act);
        skillsActButton.setOnClickListener(e -> {
            enterFragment(R.id.frame_fragment_char_skills_act,  PnPSubFragment.SUB_FRAG.CHAR_SKILLS_ACTIVE);
        });
    }


}

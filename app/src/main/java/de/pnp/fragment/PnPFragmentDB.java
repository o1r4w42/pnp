package de.pnp.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import de.pnp.R;
import de.pnp.fragment.sub.PnPSubFragment;
import de.pnp.fragment.sub.database.PnPSubFragmentDBAttrMain;
import de.pnp.fragment.sub.database.PnPSubFragmentDBAttrSub;
import de.pnp.fragment.sub.database.PnPSubFragmentDBMetaType;
import de.pnp.fragment.sub.database.PnPSubFragmentDBPriority;
import de.pnp.fragment.sub.database.PnPSubFragmentDBRace;
import de.pnp.fragment.sub.database.PnPSubFragmentDBSkillActive;
import de.pnp.fragment.sub.database.PnPSubFragmentDBSkillPassive;

public class PnPFragmentDB extends PnPFragment {

    public PnPFragmentDB() {
        super("Database");
        addFragment(new PnPSubFragmentDBAttrMain());
        addFragment(new PnPSubFragmentDBAttrSub());
        addFragment(new PnPSubFragmentDBSkillActive());
        addFragment(new PnPSubFragmentDBSkillPassive());
        addFragment(new PnPSubFragmentDBRace());
        addFragment(new PnPSubFragmentDBMetaType());
        addFragment(new PnPSubFragmentDBPriority());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_db,
                container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button initButton = view.findViewById(R.id.button_fragment_db_init);
        initButton.setOnClickListener(e -> {
            if (!dbHandler.initDefaults()) return;
            closeSubFragment();
            alertHandler.info(getString(R.string.created), getString(R.string.i_created_db_defaults));
        });

        Button priorityButton = view.findViewById(R.id.button_fragment_db_priorities);
        priorityButton.setOnClickListener(e -> {
            enterFragment(R.id.frame_fragment_db_priorities, PnPSubFragment.SUB_FRAG.DB_PRIORITIES);
        });

        Button mainAttrButton = view.findViewById(R.id.button_fragment_db_attr_main);
        mainAttrButton.setOnClickListener(e -> {
            enterFragment(R.id.frame_fragment_db_attr_main, PnPSubFragment.SUB_FRAG.DB_ATTR_MAIN);
        });

        Button subAttrButton = view.findViewById(R.id.button_fragment_db_attr_sub);
        subAttrButton.setOnClickListener(e -> {
            enterFragment(R.id.frame_fragment_db_attr_sub, PnPSubFragment.SUB_FRAG.DB_ATTR_SUB);
        });

        Button raceButton = view.findViewById(R.id.button_fragment_db_race);
        raceButton.setOnClickListener(e -> {
            enterFragment(R.id.frame_fragment_db_race, PnPSubFragment.SUB_FRAG.DB_RACE);
        });

        Button metaButton = view.findViewById(R.id.button_fragment_db_metaType);
        metaButton.setOnClickListener(e -> {
            enterFragment(R.id.frame_fragment_db_metaType, PnPSubFragment.SUB_FRAG.DB_META);
        });

        Button actSkillButton = view.findViewById(R.id.button_fragment_db_skill_act);
        actSkillButton.setOnClickListener(e -> {
            enterFragment(R.id.frame_fragment_db_skill_act, PnPSubFragment.SUB_FRAG.DB_SKILL_ACT);
        });

        Button passiveSkillButton = view.findViewById(R.id.button_fragment_db_skill_pass);
        passiveSkillButton.setOnClickListener(e -> {
            enterFragment(R.id.frame_fragment_db_skill_pass, PnPSubFragment.SUB_FRAG.DB_SKILL_PASSIVE);
        });

    }

}

package de.pnp.fragment;

import android.support.v4.app.FragmentManager;
import android.util.Log;

import java.util.HashMap;

import de.pnp.PnPActivity;
import de.pnp.PnPAlertHandler;
import de.pnp.PnPLogger;
import de.pnp.R;
import de.pnp.database.PnPDBHandler;

public class PnPHandlerFragment implements PnPLogger {

    private FragmentManager manager;

    private HashMap<Integer, PnPFragment> fragmentHashMap;
    private PnPFragment currentFragment;

    public PnPHandlerFragment(FragmentManager manager) {
        this.manager = manager;
        fragmentHashMap = new HashMap<>();
        addFragment(new PnPFragmentMain(), R.id.menu_item_main);
        addFragment(new PnPFragmentCharacter(), R.id.menu_item_char);
        addFragment(new PnPFragmentDB(), R.id.menu_item_db);
        enterFragment(R.id.menu_item_db);
    }

    private void addFragment(PnPFragment fragment, int id) {
        fragmentHashMap.put(id, fragment);
    }

    public void enterFragment(int id) {
        PnPFragment fragment = fragmentHashMap.get(id);
        if (fragment == null) {
            e("fragment with id '" + id + "' not found!");
            return;
        }
        if (currentFragment == fragment) return;
        currentFragment = fragment;
        manager.beginTransaction().replace(R.id.frame_main, fragment).commit();
    }

    public FragmentManager getManager() {
        return manager;
    }

}

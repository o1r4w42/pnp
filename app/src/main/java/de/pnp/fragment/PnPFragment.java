package de.pnp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import java.util.HashMap;

import javax.inject.Inject;

import de.pnp.PnPActivity;
import de.pnp.PnPAlertHandler;
import de.pnp.PnPLogger;
import de.pnp.database.PnPDBHandler;
import de.pnp.fragment.sub.PnPSubFragment;

public abstract class PnPFragment extends Fragment implements PnPLogger{


//    public enum FRAG {
//        DB, CHAR, MAIN;
//    }

//    private final FRAG index;
    private final String title;

    protected PnPActivity activity;

    @Inject
    protected PnPAlertHandler alertHandler;
    @Inject
    protected PnPDBHandler dbHandler;
    @Inject
    protected PnPHandlerFragment fragmentHandler;

    private FragmentManager manager;
    private HashMap<PnPSubFragment.SUB_FRAG, PnPSubFragment> subFragmentHashMap;
    private PnPSubFragment currentFragment;

//    public PnPFragment(FRAG index, String title) {
//        this.index = index;
//        this.title = title;
//        subFragmentHashMap = new HashMap<>();
//    }
    public PnPFragment(String title) {
//        this.index = index;
        this.title = title;
        subFragmentHashMap = new HashMap<>();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (PnPActivity) getActivity();
        activity.getInjector().inject(this);
        manager = fragmentHandler.getManager();
    }

    @Override
    public void onStart() {
        super.onStart();
        activity.setTitle(title);
    }

    protected void addFragment(PnPSubFragment fragment) {
        subFragmentHashMap.put(fragment.getIndex(), fragment);
    }

    protected void enterFragment(int placeId, PnPSubFragment.SUB_FRAG index) {
        PnPSubFragment fragment = subFragmentHashMap.get(index);
        if (fragment == null) {
            e("sub fragment with id '" + index + "' not found!");
            return;
        }

        if (fragment.isAdded()) {
            manager.beginTransaction().remove(fragment).commit();
            return;
        }

        FragmentTransaction ft = manager.beginTransaction();
        ft.add(placeId, fragment);
        if (currentFragment != null && currentFragment != fragment) {
            ft.remove(currentFragment);
        }
        currentFragment = fragment;
        ft.commit();
    }

    protected void closeSubFragment() {
        if (currentFragment == null)return;
        FragmentTransaction ft = manager.beginTransaction();
        ft.remove(currentFragment);
        currentFragment = null;
        ft.commit();
    }

//    public FRAG getIndex() {
//        return index;
//    }
}

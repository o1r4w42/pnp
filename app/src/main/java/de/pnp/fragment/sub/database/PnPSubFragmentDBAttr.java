package de.pnp.fragment.sub.database;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableLayout;

import java.util.List;

import de.pnp.PnPUtil;
import de.pnp.R;
import de.pnp.database.entity.attribute.PnPEntityAttributeDef;
import de.pnp.fragment.sub.PnPSubFragmentEntity;

public abstract class PnPSubFragmentDBAttr<T extends PnPEntityAttributeDef> extends PnPSubFragmentEntity<T> {

    private TableLayout table;

    public PnPSubFragmentDBAttr(SUB_FRAG index) {
        super(index, R.layout.fragment_db_attr_defs);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EditText nameField = view.findViewById(R.id.editText_fragment_db_attr_main_name);
        ImageButton buttonAttrDefAdd = view.findViewById(R.id.button_fragment_db_attr_main_add);
        buttonAttrDefAdd.setOnClickListener(e -> {
            if (!createCheck(nameField.getText().toString()))
                return;
            nameField.setText(PnPUtil.PnPStrings.EMPTY);
            activity.hideInput();
            loadTable();
        });

        table = view.findViewById(R.id.table_fragment_db_attr_main);
        table.setBackgroundColor(activity.getResources().getColor(R.color.colorBorder));
    }

    @Override
    public void onResume() {
        super.onResume();
        loadTable();
    }

    @Override
    protected void deleted() {
        loadTable();
    }

    protected void loadTable() {
        table.removeAllViews();
        List<T> attributeDefs = getAll();
        for (T attributeDef : attributeDefs) {
            createTableRow(attributeDef);
        }
    }


    private void createTableRow(T attributeDef) {
        PnPUtil.createTableRow(activity, table, borderTopLayout, getString(R.string.name));

        EditText nameField = new EditText(getContext());
        nameField.setText(attributeDef.getName());

        PnPUtil.createTableRow(activity, table, nameField);

        View.OnClickListener updateListener = e -> {
            if (updateCheck(attributeDef, nameField.getText().toString())) {
                activity.hideInput();
                return;
            }
            nameField.setText(attributeDef.getName());
        };
        View.OnClickListener deleteListener = e -> {
            delete(attributeDef);
        };
        View.OnClickListener upListener = e -> {
            if (moveCheck(attributeDef, true)) loadTable();
        };
        View.OnClickListener downListener = e -> {
            if (moveCheck(attributeDef, false)) loadTable();
        };

        PnPUtil.createButtonRow(activity, table, updateListener, deleteListener, upListener, downListener);

        PnPUtil.createTableDiv(activity, table, borderTopLayout);
    }

    private boolean createCheck(String name) {
        if (name == null || name.isEmpty()) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_empty_name));
            return false;
        }
        return create(name);
    }

    private boolean updateCheck(T attributeDef, String name) {
        if (name == null || name.isEmpty()) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_empty_name));
            return false;
        }
        if (attributeDef.equals(name)) {
            return false;
        }
        return update(attributeDef, name);
    }

    private boolean moveCheck(T attributeDef, boolean up) {
        T other = up ? findNext(attributeDef) : findPrev(attributeDef);
        if (other == null) {
            v("could not find other for level " + attributeDef.getLevel());
            return false;
        }
        int otherLevel = other.getLevel();
        other.setLevel(attributeDef.getLevel());
        attributeDef.setLevel(otherLevel);
        return move(attributeDef, other);
    }

    private T findNext(T attributeDef) {
        T other = null;
        List<T> attributes = getAll();
        for (T attrDef : attributes) {
            if (attrDef.getLevel() < attributeDef.getLevel()) {
                other = attrDef;
            } else if (attrDef == attributeDef) break;
        }
        return other;
    }

    private T findPrev(T attributeDef) {
        List<T> attributes = getAll();
        for (T attrDef : attributes) {
            if (attrDef.getLevel() > attributeDef.getLevel()) {
                return attrDef;
            }
        }
        return null;
    }

    protected abstract List<T> getAll();

    protected abstract boolean create(String name);

    protected abstract boolean update(T attributeDef, String name);

    protected abstract boolean move(T attributeDef, T other);

    protected abstract void delete(T attributeDef);


}

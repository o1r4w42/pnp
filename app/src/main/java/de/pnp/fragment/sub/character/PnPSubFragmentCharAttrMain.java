package de.pnp.fragment.sub.character;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import de.pnp.PnPUtil;
import de.pnp.R;
import de.pnp.database.entity.attribute.PnPEntityAttributeDefMain;
import de.pnp.database.handler.PnPEntityHandlerAttributeDefMain;
import de.pnp.fragment.sub.PnPSubFragment;

public class PnPSubFragmentCharAttrMain extends PnPSubFragment {

    @Inject
    PnPEntityHandlerAttributeDefMain defHandler;
    private TableLayout table;

    public PnPSubFragmentCharAttrMain() {
        super(SUB_FRAG.CHAR_ATTR_MAIN, R.layout.fragment_char_attr_main);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity.getInjector().inject(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        table = view.findViewById(R.id.table_fragment_char_attr_main);
        table.setBackgroundColor(activity.getResources().getColor(R.color.colorBorder));
    }

    @Override
    public void onResume() {
        super.onResume();
        loadTable();
    }

    private void loadTable() {
        table.removeAllViews();

        PnPUtil.createTableHead(activity, table, getString(R.string.name),
                getString(R.string.value), getString(R.string.xp));

        for (PnPEntityAttributeDefMain attributeDef : defHandler.getAll()) {

            TextView nameField = new TextView(getContext());
            nameField.setText(attributeDef.getName());

            TextView valueField = new TextView(getContext());
//            valueField.setText(Integer.toString(attributeDef.getValue()));

            TextView xpField = new TextView(getContext());
//            valueField.setText(Integer.toString(attributeDef.getValue()));

            PnPUtil.createTableRow(activity, table, nameField, valueField, xpField);
        }
    }
}

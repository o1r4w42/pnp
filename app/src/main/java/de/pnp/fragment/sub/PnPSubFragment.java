package de.pnp.fragment.sub;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;

import javax.inject.Inject;

import de.pnp.PnPActivity;
import de.pnp.PnPAlertHandler;
import de.pnp.PnPLogger;
import de.pnp.R;
import de.pnp.database.PnPDBHandler;

public abstract class PnPSubFragment extends Fragment implements PnPLogger {

    protected static final TableLayout.LayoutParams borderBotLayout =
            new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
    protected static final TableLayout.LayoutParams borderTopLayout =
            new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);

    public enum SUB_FRAG {
        CHAR_SKILLS_ACTIVE, CHAR_ATTR_MAIN, CHAR_ATTR_SUB, CHAR_PERSONAL,
        DB_PRIORITIES, DB_ATTR_MAIN, DB_ATTR_SUB, DB_RACE, DB_META, DB_SKILL_ACT, DB_SKILL_PASSIVE
    }

    private final SUB_FRAG index;
    private final int id;

    protected PnPActivity activity;

    @Inject
    protected PnPDBHandler dbHandler;
    @Inject
    protected PnPAlertHandler alertHandler;

    public PnPSubFragment(SUB_FRAG index, int id) {
        this.index = index;
        this.id = id;
        borderBotLayout.setMargins(0, 0, 0, 2);
        borderTopLayout.setMargins(0, 2, 0, 0);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (PnPActivity) getActivity();
        activity.getInjector().inject(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(id, container, false);
    }

    public SUB_FRAG getIndex() {
        return index;
    }
}

package de.pnp.fragment.sub.character;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.pnp.R;
import de.pnp.fragment.sub.PnPSubFragment;

public class PnPSubFragmentCharSkillsActive extends PnPSubFragment {

    public PnPSubFragmentCharSkillsActive() {
        super(SUB_FRAG.CHAR_SKILLS_ACTIVE, R.layout.fragment_char_skills);
    }

//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return inflater.inflate(R.layout.fragment_char_skills,
//                container, false);
//    }
}

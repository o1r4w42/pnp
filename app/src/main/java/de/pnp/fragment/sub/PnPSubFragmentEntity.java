package de.pnp.fragment.sub;

import android.view.View;

import de.pnp.R;
import de.pnp.database.entity.PnPEntity;
import de.pnp.database.handler.PnPEntityHandler;

public abstract class PnPSubFragmentEntity<T extends PnPEntity> extends PnPSubFragment {

    protected PnPEntityDeleteListener deleteListener;

    public PnPSubFragmentEntity(SUB_FRAG index, int id) {
        super(index, id);
    }

    @Override
    public void onStart() {
        super.onStart();
        deleteListener = initDeleteListener();
    }

    protected abstract PnPEntityDeleteListener initDeleteListener();

    protected abstract void deleted();

    protected class PnPEntityDeleteListener extends  PnPAbsEntityDeleteListener<T>{

        public PnPEntityDeleteListener(PnPEntityHandler<T> entityHandler) {
            super(entityHandler);
        }
    }

    protected class PnPAbsEntityDeleteListener<S extends PnPEntity> implements View.OnClickListener {
        private final PnPEntityHandler<S> entityHandler;
        private S entity;
        private String text;

        public PnPAbsEntityDeleteListener(PnPEntityHandler<S> entityHandler){
            this.entityHandler = entityHandler;
        }

        @Override
        public void onClick(View view) {
            if (entityHandler.delete(entity)) {
                alertHandler.info(getString(R.string.deleted), text);
                deleted();
            }
        }

        public void set(S entity, String text) {
            this.entity = entity;
            this.text = text;
        }
    }
}

package de.pnp.fragment.sub.database;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import de.pnp.PnPUtil;
import de.pnp.R;
import de.pnp.database.entity.attribute.PnPEntityAttributeDefMain;
import de.pnp.database.handler.PnPEntityHandlerAttributeDefMain;
import de.pnp.database.handler.PnPEntityHandlerRace2Attribute;
import de.pnp.database.handler.PnPEntityHandlerRaceDef;
import de.pnp.database.entity.race.PnPEntityRace2Attribute;
import de.pnp.database.entity.race.PnPEntityRaceDef;
import de.pnp.fragment.sub.PnPSubFragmentEntity;

public class PnPSubFragmentDBRace extends PnPSubFragmentEntity<PnPEntityRaceDef> {

    @Inject
    PnPEntityHandlerRaceDef raceDefHandler;
    @Inject
    PnPEntityHandlerAttributeDefMain attributeDefHandler;
    @Inject
    PnPEntityHandlerRace2Attribute race2attributeHandler;
    private TableLayout raceTable;
    private PnPAbsEntityDeleteListener<PnPEntityRace2Attribute> r2aDeleteListener;
    private Spinner raceSpinner;
    private ArrayAdapter<PnPEntityRaceDef> raceSpinnerAdapter;

    public PnPSubFragmentDBRace() {
        super(SUB_FRAG.DB_RACE, R.layout.fragment_db_race);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity.getInjector().inject(this);
        r2aDeleteListener = new PnPAbsEntityDeleteListener<PnPEntityRace2Attribute>(race2attributeHandler);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EditText nameField = view.findViewById(R.id.editText_fragment_db_race_defs_name);

        ImageButton buttonRaceDefAdd = view.findViewById(R.id.button_fragment_db_race_def_add);
        buttonRaceDefAdd.setOnClickListener(e -> {
            if (!createCheck(nameField.getText().toString()))
                return;
            nameField.setText(PnPUtil.PnPStrings.EMPTY);
            activity.hideInput();
            loadTable();
        });

        raceTable = view.findViewById(R.id.table_fragment_db_race);
        raceTable.setBackgroundColor(activity.getResources().getColor(R.color.colorBorder));

        raceSpinner = view.findViewById(R.id.spinner_fragment_db_race_defs);
        raceSpinnerAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item);
        raceSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        raceSpinner.setAdapter(raceSpinnerAdapter);
        AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                loadTable();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        };
        raceSpinner.setOnItemSelectedListener(onItemSelectedListener);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadRaceSpinner();
    }

    private void loadRaceSpinner() {
        raceSpinnerAdapter.clear();
        raceSpinnerAdapter.addAll(raceDefHandler.getAll());
    }

    @Override
    protected PnPEntityDeleteListener initDeleteListener() {
        return new PnPEntityDeleteListener(raceDefHandler);
    }

    @Override
    protected void deleted() {
        loadRaceSpinner();
    }

    protected void loadTable() {
        raceTable.removeAllViews();

        PnPEntityRaceDef raceDef = (PnPEntityRaceDef) raceSpinner.getSelectedItem();

        PnPUtil.createTableRow(activity, raceTable, borderTopLayout, getString(R.string.name));

        EditText nameField = new EditText(getContext());
        nameField.setText(raceDef.getName());
        PnPUtil.createTableRow(activity, raceTable, nameField);

        View.OnClickListener updateListener = e -> {
            if (updateCheck(raceDef, nameField.getText().toString())) {
                activity.hideInput();
                return;
            }
            nameField.setText(raceDef.getName());
        };
        View.OnClickListener deleteListener = e -> {
            delete(raceDef);
        };
        PnPUtil.createButtonRow(activity, raceTable, updateListener, deleteListener);

        createRaceTableSubHead(raceDef);

        PnPUtil.createTableDiv(activity, raceTable, borderTopLayout);
    }

    private void createRaceTableSubHead(PnPEntityRaceDef raceDef) {
        PnPUtil.createTableHead(activity, raceTable, getString(R.string.new_main_attribute_value));

        PnPUtil.createTableRow(activity, raceTable, getString(R.string.attribute),
                getString(R.string.minValue),getString(R.string.maxValue));

        Spinner attrSpinner = new Spinner(activity);
        ArrayAdapter<PnPEntityAttributeDefMain> attrSpinnerAdapter =
                new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item);
        attrSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        attrSpinner.setAdapter(attrSpinnerAdapter);
        attrSpinnerAdapter.addAll(attributeDefHandler.getAll());

        EditText minField = new EditText(getContext());
        minField.setInputType(InputType.TYPE_CLASS_NUMBER);

        EditText maxField = new EditText(getContext());
        maxField.setInputType(InputType.TYPE_CLASS_NUMBER);

        PnPUtil.createTableRow(activity, raceTable, attrSpinner, minField, maxField);

        ImageButton addButton = new ImageButton(getContext());
        addButton.setImageResource(android.R.drawable.ic_input_add);
        addButton.setOnClickListener(e -> {
            createCheck(raceDef, (PnPEntityAttributeDefMain) attrSpinner.getSelectedItem(),
                    minField.getText().toString(),maxField.getText().toString());
        });

        PnPUtil.createTableRow(activity, raceTable, addButton);

        PnPUtil.createTableHead(activity, raceTable, getString(R.string.main_attribute_defs));
        List<PnPEntityRace2Attribute> r2as = race2attributeHandler.getAll(raceDef);
        for (PnPEntityRace2Attribute r2a : r2as) {
            createRace2AttrRow(raceDef, r2a);
        }
    }

    private void createRace2AttrRow(PnPEntityRaceDef raceDef, PnPEntityRace2Attribute r2a) {
        PnPUtil.createTableRow(activity, raceTable, getString(R.string.attribute),
                getString(R.string.minValue), getString(R.string.maxValue));

        TextView attrField = new TextView(getContext());
        attrField.setText(r2a.getAttributeDef().getName());

        EditText minField = new EditText(getContext());
        minField.setInputType(InputType.TYPE_CLASS_NUMBER);
        minField.setText(Integer.toString(r2a.getMinValue()));

        EditText maxField = new EditText(getContext());
        maxField.setInputType(InputType.TYPE_CLASS_NUMBER);
        maxField.setText(Integer.toString(r2a.getMaxValue()));

        PnPUtil.createTableRow(activity, raceTable, attrField, minField, maxField);

        View.OnClickListener updateListener = e -> {
            updateCheck(r2a,raceDef,  minField.getText().toString(),  maxField.getText().toString());
        };
        View.OnClickListener deleteListener = e -> {
            delete(r2a);
        };
        PnPUtil.createButtonRow(activity, raceTable, updateListener, deleteListener);
    }

    private boolean createCheck(String name) {
        if (name == null || name.isEmpty()) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_empty_name));
            return false;
        }
        if (raceDefHandler.exists(name)) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_duplicate_name) + name + getString(R.string.i_end));
            return false;
        }
        if (!raceDefHandler.create(name)) {
            return false;
        }
        alertHandler.info(getString(R.string.created),
                getString(R.string.i_created_race_def) + name + getString(R.string.i_end));
        return true;
    }

    private boolean updateCheck(PnPEntityRaceDef raceDef, String name) {
        if (name == null || name.isEmpty()) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_empty_name));
            return false;
        }
        if (raceDefHandler.exists(name)) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_duplicate_name) + name + getString(R.string.i_end));
            return false;
        }
        if (raceDef.equals(name)) return false;
        if (!raceDefHandler.update(raceDef)) return false;
        alertHandler.info(getString(R.string.updated), getString(R.string.i_updated_race_def) + name + getString(R.string.i_end));
        return true;
    }

    private void delete(PnPEntityRaceDef raceDef) {
        deleteListener.set(raceDef,
                getString(R.string.i_deleted_race_def) + raceDef.getName() + getString(R.string.i_end));
        alertHandler.confirm(getString(R.string.delete),
                getString(R.string.q_delete_race_def) + raceDef.getName() +
                        getString(R.string.q_end), deleteListener);
    }

    private boolean createCheck(PnPEntityRaceDef raceDef, PnPEntityAttributeDefMain attributeDef, String min, String max) {
        if (min == null || min.isEmpty() || max == null || max.isEmpty()) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_empty_value));
            return false;
        }
        if (race2attributeHandler.getEntity(raceDef, attributeDef) != null) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_duplicate_race2attr));
            return false;
        }
        if (!race2attributeHandler.create(new PnPEntityRace2Attribute(attributeDef,
                Integer.parseInt(min), Integer.parseInt(max), raceDef)))
            return false;
        loadTable();
        alertHandler.info(getString(R.string.created),
                getString(R.string.i_created_r2a) + attributeDef.getName() + getString(R.string.i_end));
        return true;
    }

    private boolean updateCheck(PnPEntityRace2Attribute r2a, PnPEntityRaceDef raceDef, String min, String max) {
        int minValue = Integer.parseInt(min);
        int maxValue = Integer.parseInt(max);
        if (r2a.equals(r2a.getAttributeDef(), minValue, maxValue, raceDef)) {
            return false;
        }
        r2a.setMinValue(minValue);
        r2a.setMaxValue(maxValue);
        if (!race2attributeHandler.update(r2a)) return false;
        alertHandler.info(getString(R.string.updated), getString(R.string.i_updated_r2a) +
                r2a.getAttributeDef().getName() + getString(R.string.i_end));
        return true;
    }

    private void delete(PnPEntityRace2Attribute r2a) {
        r2aDeleteListener.set(r2a,
                getString(R.string.i_deleted_r2a) + r2a.getAttributeDef().getName() + getString(R.string.i_end));
        alertHandler.confirm(getString(R.string.delete),
                getString(R.string.q_delete_r2a_start) + r2a.getRaceDef().getName()
                        + getString(R.string.q_delete_r2a_mid) + r2a.getAttributeDef().getName() +
                        getString(R.string.q_end), r2aDeleteListener);
    }
}
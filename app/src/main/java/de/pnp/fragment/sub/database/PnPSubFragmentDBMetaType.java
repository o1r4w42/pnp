package de.pnp.fragment.sub.database;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import de.pnp.PnPUtil;
import de.pnp.R;
import de.pnp.database.entity.attribute.PnPEntityAttributeDefMain;
import de.pnp.database.handler.PnPEntityHandlerAttributeDefMain;
import de.pnp.database.handler.PnPEntityHandlerMetaType2Attribute;
import de.pnp.database.handler.PnPEntityHandlerMetaTypeDef;
import de.pnp.database.handler.PnPEntityHandlerRace2Attribute;
import de.pnp.database.handler.PnPEntityHandlerRaceDef;
import de.pnp.database.entity.race.PnPEntityMetaType2Attribute;
import de.pnp.database.entity.race.PnPEntityMetaTypeDef;
import de.pnp.database.entity.race.PnPEntityRace2Attribute;
import de.pnp.database.entity.race.PnPEntityRaceDef;
import de.pnp.fragment.sub.PnPSubFragmentEntity;

public class PnPSubFragmentDBMetaType extends PnPSubFragmentEntity<PnPEntityMetaTypeDef> {

    @Inject
    PnPEntityHandlerRaceDef raceDefHandler;
    @Inject
    PnPEntityHandlerMetaTypeDef metaDefHandler;
    @Inject
    PnPEntityHandlerAttributeDefMain attributeDefHandler;
    @Inject
    PnPEntityHandlerRace2Attribute race2attributeHandler;
    @Inject
    PnPEntityHandlerMetaType2Attribute metaType2attributeHandler;
    private TableLayout metaTable;
    private ArrayAdapter<PnPEntityRaceDef> raceSpinnerAdapter;
    private ArrayAdapter<PnPEntityMetaTypeDef> metaSpinnerAdapter;
    private PnPAbsEntityDeleteListener<PnPEntityMetaType2Attribute> m2aDeleteListener;
    private Spinner metaSpinner;

    public PnPSubFragmentDBMetaType() {
        super(SUB_FRAG.DB_META, R.layout.fragment_db_metatype);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity.getInjector().inject(this);
        m2aDeleteListener = new PnPAbsEntityDeleteListener<PnPEntityMetaType2Attribute>(metaType2attributeHandler);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        EditText metaNameField = view.findViewById(R.id.editText_fragment_db_metatype_def_name);

        Spinner raceSpinner = view.findViewById(R.id.spinner_fragment_db_metatype_def_race);
        raceSpinnerAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item);
        raceSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        raceSpinner.setAdapter(raceSpinnerAdapter);

        ImageButton buttonMetaDefAdd = view.findViewById(R.id.button_fragment_db_metatype_def_add);
        buttonMetaDefAdd.setOnClickListener(e -> {
            if (!createCheck(metaNameField.getText().toString(),
                    (PnPEntityRaceDef) raceSpinner.getSelectedItem()))
                return;
            metaNameField.setText(PnPUtil.PnPStrings.EMPTY);
            activity.hideInput();
            loadTable();
        });

        metaTable = view.findViewById(R.id.table_fragment_db_matatype);
        metaTable.setBackgroundColor(activity.getResources().getColor(R.color.colorBorder));

        metaSpinner = view.findViewById(R.id.spinner_fragment_db_metatype_defs);
        metaSpinnerAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item);
        metaSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        metaSpinner.setAdapter(metaSpinnerAdapter);
        AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                loadTable();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        };
        metaSpinner.setOnItemSelectedListener(onItemSelectedListener);
    }

    @Override
    public void onResume() {
        super.onResume();
        raceSpinnerAdapter.clear();
        raceSpinnerAdapter.addAll(raceDefHandler.getAll());
        loadMetaSpinner();
//        loadTable();
    }

    private void loadMetaSpinner() {
        metaSpinnerAdapter.clear();
        metaSpinnerAdapter.addAll(metaDefHandler.getAll());
    }

    @Override
    protected PnPEntityDeleteListener initDeleteListener() {
        return new PnPEntityDeleteListener(metaDefHandler);
    }

    @Override
    protected void deleted() {
        loadMetaSpinner();
    }

    protected void loadTable() {
        metaTable.removeAllViews();
        PnPEntityMetaTypeDef metaDef = (PnPEntityMetaTypeDef) metaSpinner.getSelectedItem();

        PnPUtil.createTableRow(activity, metaTable, borderTopLayout, getString(R.string.race),
                getString(R.string.name));

        EditText nameField = new EditText(getContext());
        nameField.setText(metaDef.getName());

        Spinner attrSpinner = new Spinner(activity);
        ArrayAdapter<PnPEntityRaceDef> attrSpinnerAdapter =
                new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item);
        attrSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        attrSpinner.setAdapter(attrSpinnerAdapter);
        attrSpinnerAdapter.addAll(raceDefHandler.getAll());
        attrSpinner.setSelection(attrSpinnerAdapter.getPosition(metaDef.getRaceDef()));

        PnPUtil.createTableRow(activity, metaTable, attrSpinner, nameField);

        View.OnClickListener updateListener = e -> {
            if (updateCheck(metaDef, nameField.getText().toString(),
                    (PnPEntityRaceDef) attrSpinner.getSelectedItem())) {
                activity.hideInput();
                return;
            }
            nameField.setText(metaDef.getName());
            attrSpinner.setSelection(attrSpinnerAdapter.getPosition(metaDef.getRaceDef()));
        };
        View.OnClickListener deleteListener = e -> {
            delete(metaDef);
        };
        PnPUtil.createButtonRow(activity, metaTable, updateListener, deleteListener);

        createMetaTableSubHead(metaDef);
//        List<PnPEntityAttributeDefMain> attributes = attributeDefHandler.getAll();
//        List<PnPEntityMetaTypeDef> metaDefs = metaDefHandler.getAll();
//        for (PnPEntityMetaTypeDef metaDef : metaDefs) {
//            createMetaTableRow(metaDef, attributes);
//        }
        PnPUtil.createTableDiv(activity, metaTable, borderTopLayout);
    }

//    private void createMetaTableRow(PnPEntityMetaTypeDef metaDef,
//                                    List<PnPEntityAttributeDefMain> attributes) {
//        PnPUtil.createTableRow(activity, metaTable, borderTopLayout, getString(R.string.name), getString(R.string.race));
//
//        EditText nameField = new EditText(getContext());
//        nameField.setText(metaDef.getName());
//
//        Spinner attrSpinner = new Spinner(activity);
//        ArrayAdapter<PnPEntityRaceDef> attrSpinnerAdapter =
//                new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item);
//        attrSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        attrSpinner.setAdapter(attrSpinnerAdapter);
//        attrSpinnerAdapter.addAll(raceDefHandler.getAll());
//        attrSpinner.setSelection(attrSpinnerAdapter.getPosition(metaDef.getRaceDef()));
//
//        PnPUtil.createTableRow(activity, metaTable, nameField, attrSpinner);
//
//        View.OnClickListener updateListener = e -> {
//            if (updateCheck(metaDef, nameField.getText().toString(),
//                    (PnPEntityRaceDef) attrSpinner.getSelectedItem())) {
//                activity.hideInput();
//                return;
//            }
//            nameField.setText(metaDef.getName());
//            attrSpinner.setSelection(attrSpinnerAdapter.getPosition(metaDef.getRaceDef()));
//        };
//        View.OnClickListener deleteListener = e -> {
//            delete(metaDef);
//        };
//        PnPUtil.createButtonRow(activity, metaTable, updateListener, deleteListener);
//
//        createMetaTableSubHead(metaDef, attributes);
//        PnPUtil.createTableDiv(activity, metaTable, borderTopLayout);
//    }

    private void createMetaTableSubHead(PnPEntityMetaTypeDef metaDef) {
        PnPUtil.createTableHead(activity, metaTable, getString(R.string.new_main_attribute_value));

        PnPUtil.createTableRow(activity, metaTable, getString(R.string.attribute),
                getString(R.string.minValue), getString(R.string.maxValue));

        Spinner attrSpinner = new Spinner(activity);
        ArrayAdapter<PnPEntityAttributeDefMain> attrSpinnerAdapter =
                new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item);
        attrSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        attrSpinner.setAdapter(attrSpinnerAdapter);
        attrSpinnerAdapter.addAll(attributeDefHandler.getAll());

        EditText minField = new EditText(getContext());
        minField.setInputType(InputType.TYPE_CLASS_NUMBER);

        EditText maxField = new EditText(getContext());
        maxField.setInputType(InputType.TYPE_CLASS_NUMBER);

        PnPUtil.createTableRow(activity, metaTable, attrSpinner, minField, maxField);

        ImageButton addButton = new ImageButton(getContext());
        addButton.setImageResource(android.R.drawable.ic_input_add);
        addButton.setOnClickListener(e -> {
            createCheck(metaDef, (PnPEntityAttributeDefMain) attrSpinner.getSelectedItem(),
                    minField.getText().toString(), maxField.getText().toString());
        });

        PnPUtil.createTableRow(activity, metaTable, addButton);

        PnPUtil.createTableHead(activity, metaTable, getString(R.string.main_attribute_defs));

        for (PnPEntityMetaType2Attribute m2a : metaType2attributeHandler.getEntity(metaDef)) {
            createMeta2AttributeRow(metaDef, m2a);
        }
    }

    private void createMeta2AttributeRow(PnPEntityMetaTypeDef metaDef, PnPEntityMetaType2Attribute m2a) {
        PnPUtil.createTableRow(activity, metaTable, getString(R.string.attribute),
                getString(R.string.minValue), getString(R.string.maxValue));

        TextView attrField = new TextView(getContext());
        attrField.setText(m2a.getAttributeDef().getName());

        EditText minField = new EditText(getContext());
        minField.setInputType(InputType.TYPE_CLASS_NUMBER);
        minField.setText(Integer.toString(m2a.getMinValue()));

        EditText maxField = new EditText(getContext());
        maxField.setInputType(InputType.TYPE_CLASS_NUMBER);
        maxField.setText(Integer.toString(m2a.getMaxValue()));

        PnPUtil.createTableRow(activity, metaTable, attrField, minField, maxField);

        View.OnClickListener updateListener = e -> {
            updateCheck(m2a, metaDef, m2a.getAttributeDef(), minField.getText().toString(), maxField.getText().toString());
        };
        View.OnClickListener deleteListener = e -> {
            delete(m2a);
        };
        PnPUtil.createButtonRow(activity, metaTable, updateListener, deleteListener);
    }

    private boolean createCheck(String name, PnPEntityRaceDef raceDef) {
        if (name == null || name.isEmpty()) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_empty_name));
            return false;
        }
        if (metaDefHandler.exists(name)) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_duplicate_name) + name + getString(R.string.i_end));
            return false;
        }

        if (!metaDefHandler.create(name, raceDef)) {
            return false;
        }
        alertHandler.info(getString(R.string.created), getString(R.string.i_created_metaType_def) + name + getString(R.string.i_end));
        return true;
    }

    private boolean updateCheck(PnPEntityMetaTypeDef metaDef, String name, PnPEntityRaceDef raceDef) {
        if (name == null || name.isEmpty()) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_empty_name));
            return false;
        }
        if (metaDefHandler.exists(name)) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_duplicate_name) + name + getString(R.string.i_end));
            return false;
        }
        if (metaDef.equals(name, raceDef)) return false;
        if (!metaDefHandler.update(metaDef)) return false;
        alertHandler.info(getString(R.string.updated), getString(R.string.i_updated_metaType_def) + name + getString(R.string.i_end));
        return true;
    }

    private void delete(PnPEntityMetaTypeDef metaDef) {
        deleteListener.set(metaDef, getString(R.string.i_deleted_main_attr_def) + metaDef.getName() +
                getString(R.string.i_end));
        alertHandler.confirm(getString(R.string.delete),
                getString(R.string.q_delete_metaType_def) + metaDef.getName() +
                        getString(R.string.q_end), deleteListener);
    }

    private boolean createCheck(PnPEntityMetaTypeDef metaDef, PnPEntityAttributeDefMain attributeDef, String min, String max) {
        if (min == null || min.isEmpty() || max == null || max.isEmpty()) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_empty_value));
            return false;
        }
        if (metaType2attributeHandler.getEntity(metaDef, attributeDef) != null) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_duplicate_meta2attr));
            return false;
        }
        if (!metaType2attributeHandler.create(new PnPEntityMetaType2Attribute(attributeDef,
                Integer.parseInt(min), Integer.parseInt(max), metaDef)))
            return false;
        loadTable();
        alertHandler.info(getString(R.string.created),
                getString(R.string.i_created_m2a) + attributeDef.getName() + getString(R.string.i_end));
        return true;
    }

    private boolean updateCheck(PnPEntityMetaType2Attribute m2a, PnPEntityMetaTypeDef metaDef,
                                PnPEntityAttributeDefMain attributeDef, String min, String max) {
        int minValue = Integer.parseInt(min);
        int maxValue = Integer.parseInt(max);
        if (m2a.equals(attributeDef, minValue, maxValue, metaDef)) {
            return false;
        }
        m2a.setMinValue(minValue);
        m2a.setMaxValue(maxValue);
        if (!metaType2attributeHandler.update(m2a)) return false;
        alertHandler.info(getString(R.string.updated), getString(R.string.i_updated_m2a) +
                m2a.getAttributeDef().getName() + getString(R.string.i_end));
        return true;
    }

    private void delete(PnPEntityMetaType2Attribute m2a) {
        m2aDeleteListener.set(m2a,
                getString(R.string.i_deleted_m2a) + m2a.getAttributeDef().getName() + getString(R.string.i_end));
        alertHandler.confirm(getString(R.string.delete),
                getString(R.string.q_delete_m2a_start) + m2a.getMetaDef().getName()
                        + getString(R.string.q_delete_r2a_mid) + m2a.getAttributeDef().getName() +
                        getString(R.string.q_end), m2aDeleteListener);
    }
}
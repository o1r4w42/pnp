package de.pnp.fragment.sub.database;

import android.os.Bundle;
import android.support.annotation.Nullable;

import java.util.List;

import javax.inject.Inject;

import de.pnp.R;
import de.pnp.database.entity.attribute.PnPEntityAttributeDefMain;
import de.pnp.database.handler.PnPEntityHandlerSkillActive;
import de.pnp.database.entity.skill.PnPEntitySkillDefActive;

public class PnPSubFragmentDBSkillActive extends PnPSubFragmentDBSkill<PnPEntitySkillDefActive> {

    @Inject
    PnPEntityHandlerSkillActive defHandler;

    public PnPSubFragmentDBSkillActive() {
        super(SUB_FRAG.DB_SKILL_ACT);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity.getInjector().inject(this);
    }

    @Override
    protected List<PnPEntitySkillDefActive> getAll() {
        return defHandler.getAll();
    }


    @Override
    protected PnPEntityDeleteListener initDeleteListener() {
        return new PnPEntityDeleteListener(defHandler);
    }

    @Override
    protected boolean create(String name, PnPEntityAttributeDefMain attributeDef) {
        if (defHandler.exists(name)) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_duplicate_name) + name + getString(R.string.i_end));
            return false;
        }
        if(!defHandler.create(name, attributeDef)){
            return false;
        }
        alertHandler.info(getString(R.string.created), getString(R.string.i_created_main_attr_def)+name+getString(R.string.i_end));
        return true;
    }

    @Override
    protected boolean update(PnPEntitySkillDefActive skillDef, String name, PnPEntityAttributeDefMain attributeDef) {
        if (defHandler.exists(name, skillDef.getId())) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_duplicate_name) + name + getString(R.string.i_end));
            return false;
        }
        skillDef.setName(name);
        skillDef.setAttribute(attributeDef);
        if(!defHandler.update(skillDef)){
            return false;
        }
        alertHandler.info(getString(R.string.updated), getString(R.string.i_updated_main_attr_def)+name+getString(R.string.i_end));
        return true;
    }

    @Override
    protected boolean move(PnPEntitySkillDefActive skillDef, PnPEntitySkillDefActive other) {
        return defHandler.update(skillDef) && defHandler.update(other);
    }

    @Override
    protected void delete(PnPEntitySkillDefActive skillDef) {
        deleteListener.set(skillDef, getString(R.string.i_deleted_main_attr_def) + skillDef.getName() + getString(R.string.i_end));
        alertHandler.confirm(getString(R.string.delete), getString(R.string.q_delete_main_attr_def) + skillDef.getName() + getString(R.string.q_end), deleteListener);
    }
}

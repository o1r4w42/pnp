package de.pnp.fragment.sub.character;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.pnp.R;
import de.pnp.fragment.sub.PnPSubFragment;

public class PnPSubFragmentCharPersonal extends PnPSubFragment {

    public PnPSubFragmentCharPersonal() {
        super(SUB_FRAG.CHAR_PERSONAL, R.layout.fragment_char_personal);
    }

//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return inflater.inflate(R.layout.fragment_char_personal,
//                container, false);
//    }
}

package de.pnp.fragment.sub.database;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import de.pnp.R;
import de.pnp.database.entity.attribute.PnPEntityAttributeDefSub;
import de.pnp.database.handler.PnPEntityHandlerAttributeDefSub;

public class PnPSubFragmentDBAttrSub extends PnPSubFragmentDBAttr<PnPEntityAttributeDefSub> {

    @Inject
    PnPEntityHandlerAttributeDefSub attributeDefHandler;

    public PnPSubFragmentDBAttrSub() {
        super(SUB_FRAG.DB_ATTR_SUB);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity.getInjector().inject(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView headingNew = view.findViewById(R.id.textView_fragment_db_attr_main_heading_new);
        headingNew.setText(getString(R.string.new_sub_attribute_def));

        TextView heading = view.findViewById(R.id.textView_fragment_db_attr_main_heading);
        heading.setText(getString(R.string.sub_attribute_defs));
    }

    @Override
    protected List<PnPEntityAttributeDefSub> getAll() {
        return attributeDefHandler.getAll();
    }

    @Override
    protected PnPEntityDeleteListener initDeleteListener() {
        return new PnPEntityDeleteListener(attributeDefHandler);
    }

    @Override
    protected boolean create(String name) {
        if (attributeDefHandler.exists(name)) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_duplicate_name) + name + getString(R.string.i_end));
            return false;
        }
        if (!attributeDefHandler.create(name)) {
            return false;
        }
        alertHandler.info(getString(R.string.created), getString(R.string.i_created_sub_attr_def) + name + getString(R.string.i_end));
        return true;
    }

    @Override
    protected boolean update(PnPEntityAttributeDefSub attributeDef, String name) {
        if (attributeDefHandler.exists(name, attributeDef.getId())) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_duplicate_name) + name + getString(R.string.i_end));
            return false;
        }
        attributeDef.setName(name);
        if (!attributeDefHandler.update(attributeDef)) {
            return false;
        }
        alertHandler.info(getString(R.string.updated), getString(R.string.i_updated_sub_attr_def) + name + getString(R.string.i_end));
        return true;
    }

    @Override
    protected boolean move(PnPEntityAttributeDefSub attributeDef, PnPEntityAttributeDefSub other) {
        return attributeDefHandler.update(attributeDef) &&
                attributeDefHandler.update(other);
    }

    @Override
    protected void delete(PnPEntityAttributeDefSub attributeDef) {
        deleteListener.set(attributeDef,
                getString(R.string.i_deleted_main_attr_def) + attributeDef.getName() + getString(R.string.i_end));
        alertHandler.confirm(getString(R.string.delete),
                getString(R.string.q_delete_sub_attr_def) + attributeDef.getName() + getString(R.string.q_end), deleteListener);
    }
}

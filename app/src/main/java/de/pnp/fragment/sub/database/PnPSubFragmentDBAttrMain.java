package de.pnp.fragment.sub.database;

import android.os.Bundle;
import android.support.annotation.Nullable;

import java.util.List;

import javax.inject.Inject;

import de.pnp.R;
import de.pnp.database.entity.attribute.PnPEntityAttributeDefMain;
import de.pnp.database.handler.PnPEntityHandlerAttributeDefMain;
import de.pnp.database.handler.PnPEntityHandlerMetaType2Attribute;
import de.pnp.database.handler.PnPEntityHandlerRace2Attribute;

public class PnPSubFragmentDBAttrMain extends PnPSubFragmentDBAttr<PnPEntityAttributeDefMain> {

    @Inject
    PnPEntityHandlerAttributeDefMain defHandler;
    @Inject
    PnPEntityHandlerRace2Attribute race2AttributeHandler;
    @Inject
    PnPEntityHandlerMetaType2Attribute metaType2AttributeHandler;

    public PnPSubFragmentDBAttrMain() {
        super(SUB_FRAG.DB_ATTR_MAIN);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity.getInjector().inject(this);
    }

    @Override
    protected List<PnPEntityAttributeDefMain> getAll() {
        return defHandler.getAll();
    }

    @Override
    protected PnPEntityDeleteListener initDeleteListener() {
        return new PnPEntityDeleteListener(defHandler);
    }


    @Override
    protected boolean create(String name) {
        if (defHandler.exists(name)) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_duplicate_name) + name + getString(R.string.i_end));
            return false;
        }
        if (!defHandler.create(name)) {
            return false;
        }
        alertHandler.info(getString(R.string.created), getString(R.string.i_created_main_attr_def) + name + getString(R.string.i_end));
        return true;
    }

    @Override
    protected boolean update(PnPEntityAttributeDefMain attributeDef, String name) {
        if (defHandler.exists(name, attributeDef.getId())) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_duplicate_name) + name + getString(R.string.i_end));
            return false;
        }
        attributeDef.setName(name);
        if (!defHandler.update(attributeDef)) {
            return false;
        }
        alertHandler.info(getString(R.string.updated), getString(R.string.i_updated_main_attr_def) + name + getString(R.string.i_end));
        return true;
    }

    @Override
    protected boolean move(PnPEntityAttributeDefMain attributeDef, PnPEntityAttributeDefMain other) {
        return defHandler.update(attributeDef) && defHandler.update(other);
    }

    @Override
    protected void delete(PnPEntityAttributeDefMain attributeDef) {
        if (race2AttributeHandler.getAll(attributeDef).size() > 0) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_attr_in_use_by_race));
            return;
        }
        if (metaType2AttributeHandler.getAll(attributeDef).size() > 0) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_attr_in_use_by_meta));
            return;
        }
        deleteListener.set(attributeDef,
                getString(R.string.i_deleted_main_attr_def) + attributeDef.getName() + getString(R.string.i_end));
        alertHandler.confirm(getString(R.string.delete),
                getString(R.string.q_delete_main_attr_def) + attributeDef.getName() +
                        getString(R.string.q_end), deleteListener);
    }

}

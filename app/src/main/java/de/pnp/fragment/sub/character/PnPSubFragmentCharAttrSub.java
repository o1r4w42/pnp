package de.pnp.fragment.sub.character;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.pnp.R;
import de.pnp.fragment.sub.PnPSubFragment;

public class PnPSubFragmentCharAttrSub extends PnPSubFragment {

    public PnPSubFragmentCharAttrSub() {
        super(SUB_FRAG.CHAR_ATTR_SUB, R.layout.fragment_char_attr_sub);
    }

//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return inflater.inflate(R.layout.fragment_char_attr_sub,
//                container, false);
//    }
}

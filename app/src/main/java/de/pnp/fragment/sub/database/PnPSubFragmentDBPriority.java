package de.pnp.fragment.sub.database;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableLayout;

import javax.inject.Inject;

import de.pnp.PnPUtil;
import de.pnp.R;
import de.pnp.database.entity.priority.PnPEntityPriority;
import de.pnp.database.handler.PnPEntityHandlerPriority;
import de.pnp.fragment.sub.PnPSubFragmentEntity;

public class PnPSubFragmentDBPriority extends PnPSubFragmentEntity<PnPEntityPriority> {

    @Inject
    PnPEntityHandlerPriority priorityHandler;
    private TableLayout table;

    public PnPSubFragmentDBPriority() {
        super(SUB_FRAG.DB_PRIORITIES, R.layout.fragment_db_priority);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity.getInjector().inject(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EditText nameField = view.findViewById(R.id.editText_fragment_db_priority_name);

        ImageButton addButton = view.findViewById(R.id.button_fragment_db_priority_add);
        addButton.setOnClickListener(e -> {
            if (!createCheck(nameField.getText().toString()))
                return;
            nameField.setText(PnPUtil.PnPStrings.EMPTY);
            activity.hideInput();
            loadTable();
        });

        table = view.findViewById(R.id.table_fragment_db_priority);
        table.setBackgroundColor(activity.getResources().getColor(R.color.colorBorder));
    }

    @Override
    public void onResume() {
        super.onResume();
        loadTable();
    }

    @Override
    protected PnPEntityDeleteListener initDeleteListener() {
        return new PnPEntityDeleteListener(priorityHandler);
    }

    @Override
    protected void deleted() {
        loadTable();
    }

    protected void loadTable() {
        table.removeAllViews();
        for (PnPEntityPriority priority : priorityHandler.getAll()) {
            createRaceTableRow(priority);
        }
    }

    private void createRaceTableRow(PnPEntityPriority priority) {
        PnPUtil.createTableRow(activity, table, borderTopLayout, getString(R.string.name));

        EditText nameField = new EditText(getContext());
        nameField.setText(priority.getName());
        PnPUtil.createTableRow(activity, table, nameField);

        View.OnClickListener updateListener = e -> {
            if (updateCheck(priority, nameField.getText().toString())) {
                activity.hideInput();
                return;
            }
            nameField.setText(priority.getName());
        };
        View.OnClickListener deleteListener = e -> {
            delete(priority);
        };
        View.OnClickListener upListener = e -> {
            if (moveCheck(priority, true)) loadTable();
        };
        View.OnClickListener downListener = e -> {
            if (moveCheck(priority, false)) loadTable();
        };

        PnPUtil.createButtonRow(activity, table, updateListener, deleteListener, upListener, downListener);
    }

    private boolean createCheck(String name) {
        if (name == null || name.isEmpty()) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_empty_name));
            return false;
        }
        if (priorityHandler.exists(name)) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_duplicate_name) + name + getString(R.string.i_end));
            return false;
        }
        if (!priorityHandler.create(name)) {
            return false;
        }
        alertHandler.info(getString(R.string.created),
                getString(R.string.i_created_priority_def) + name + getString(R.string.i_end));
        return true;
    }

    private boolean updateCheck(PnPEntityPriority priority, String name) {
        if (name == null || name.isEmpty()) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_empty_name));
            return false;
        }
        if (priorityHandler.exists(name)) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_duplicate_name) + name + getString(R.string.i_end));
            return false;
        }
        if (priority.equals(name)) return false;
        if (!priorityHandler.update(priority)) return false;
        alertHandler.info(getString(R.string.updated), getString(R.string.i_updated_priority_def) + name + getString(R.string.i_end));
        return true;
    }

    private void delete(PnPEntityPriority priority) {
        deleteListener.set(priority,
                getString(R.string.i_deleted_priority_def) + priority.getName() + getString(R.string.i_end));
        alertHandler.confirm(getString(R.string.delete),
                getString(R.string.q_delete_priority_def) + priority.getName() +
                        getString(R.string.q_end), deleteListener);
    }

    private boolean moveCheck(PnPEntityPriority priority, boolean up) {
        PnPEntityPriority other = up ? findNext(priority) : findPrev(priority);
        if (other == null) {
            e("could not find other for level " + priority.getLevel());
            return false;
        }
        int otherLevel = other.getLevel();
        other.setLevel(priority.getLevel());
        priority.setLevel(otherLevel);
        return move(priority, other);
    }

    private PnPEntityPriority findNext(PnPEntityPriority attributeDef) {
        PnPEntityPriority other = null;
        for (PnPEntityPriority priority : priorityHandler.getAll()) {
            if (priority.getLevel() < attributeDef.getLevel()) {
                other = priority;
            } else if (priority == attributeDef) break;
        }
        return other;
    }

    private PnPEntityPriority findPrev(PnPEntityPriority attributeDef) {
        for (PnPEntityPriority priority : priorityHandler.getAll()) {
            if (priority.getLevel() > attributeDef.getLevel()) {
                return priority;
            }
        }
        return null;
    }

    private boolean move(PnPEntityPriority priority, PnPEntityPriority other) {
        v("move");
        return priorityHandler.update(priority) && priorityHandler.update(other);
    }
}

package de.pnp.fragment.sub.database;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TableLayout;

import java.util.List;

import javax.inject.Inject;

import de.pnp.PnPUtil;
import de.pnp.R;
import de.pnp.database.entity.attribute.PnPEntityAttributeDefMain;
import de.pnp.database.handler.PnPEntityHandlerAttributeDefMain;
import de.pnp.database.entity.skill.PnPEntitySkillDef;
import de.pnp.fragment.sub.PnPSubFragmentEntity;

public abstract class PnPSubFragmentDBSkill<T extends PnPEntitySkillDef> extends PnPSubFragmentEntity<T> {

    private ArrayAdapter<PnPEntityAttributeDefMain> attrSpinnerAdapter;

    private TableLayout table;

    @Inject
    PnPEntityHandlerAttributeDefMain attributeDefHandler;

    public PnPSubFragmentDBSkill(SUB_FRAG index) {
        super(index, R.layout.fragment_db_skill_defs);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity.getInjector().inject(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EditText nameField = view.findViewById(R.id.editText_fragment_db_skill_defs_name);

        Spinner attrSpinner = view.findViewById(R.id.spinner_fragment_db_skill_defs_attr);
        attrSpinnerAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item);
        attrSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        attrSpinner.setAdapter(attrSpinnerAdapter);

        ImageButton buttonSkillDefAdd = view.findViewById(R.id.button_fragment_db_skill_defs_add);
        buttonSkillDefAdd.setOnClickListener(e -> {
            if (!createCheck(nameField.getText().toString(), (PnPEntityAttributeDefMain) attrSpinner.getSelectedItem()))
                return;
            nameField.setText(PnPUtil.PnPStrings.EMPTY);
            activity.hideInput();
            loadTable();
        });

        table = view.findViewById(R.id.table_fragment_db_skill_defs);
        table.setBackgroundColor(activity.getResources().getColor(R.color.colorBorder));
    }

    @Override
    public void onResume() {
        super.onResume();
        attrSpinnerAdapter.clear();
        attrSpinnerAdapter.addAll(attributeDefHandler.getAll());
        loadTable();
    }

    @Override
    protected void deleted() {
        loadTable();
    }

    protected void loadTable() {
        table.removeAllViews();
        List<T> skillDefs = getAll();
        for (T skillDef : skillDefs) {
            createTableRow(skillDef);
        }
    }

    private void createTableRow(T skillDef) {
        PnPUtil.createTableRow(activity, table, borderTopLayout, getString(R.string.name),
                getString(R.string.mainAttribute));

        EditText nameField = new EditText(getContext());
        nameField.setText(skillDef.getName());

        Spinner attrSpinner = new Spinner(activity);
        ArrayAdapter<PnPEntityAttributeDefMain> attrSpinnerAdapter =
                new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item);
        attrSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        attrSpinner.setAdapter(attrSpinnerAdapter);
        attrSpinnerAdapter.addAll(attributeDefHandler.getAll());
        attrSpinner.setSelection(attrSpinnerAdapter.getPosition(skillDef.getAttribute()));

        PnPUtil.createTableRow(activity, table, nameField, attrSpinner);

        View.OnClickListener updateListener = e -> {
            if (updateCheck(skillDef, nameField.getText().toString(),
                    (PnPEntityAttributeDefMain) attrSpinner.getSelectedItem())) {
                activity.hideInput();
                return;
            }
            nameField.setText(skillDef.getName());
            attrSpinner.setSelection(attrSpinnerAdapter.getPosition(skillDef.getAttribute()));
        };
        View.OnClickListener deleteListener = e -> {
            delete(skillDef);
        };
        View.OnClickListener upListener = e -> {
            if (moveCheck(skillDef, true)) loadTable();
        };
        View.OnClickListener downListener = e -> {
            if (moveCheck(skillDef, false)) loadTable();
        };

        PnPUtil.createButtonRow(activity, table, updateListener, deleteListener, upListener, downListener);

        PnPUtil.createTableDiv(activity, table, borderTopLayout);
    }

    private boolean createCheck(String name, PnPEntityAttributeDefMain attributeDef) {
        if (name == null || name.isEmpty()) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_empty_name));
            return false;
        }
        if (attributeDef == null) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_empty_attr));
            return false;
        }
        return create(name, attributeDef);
    }

    private boolean updateCheck(T skillDef, String name, PnPEntityAttributeDefMain attributeDef) {
        if (name == null || name.isEmpty()) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_empty_name));
            return false;
        }
        if (attributeDef == null) {
            alertHandler.error(getString(R.string.error), getString(R.string.e_empty_attr));
            return false;
        }
        if (skillDef.equals(name, attributeDef)) {
            return false;
        }
        return update(skillDef, name, attributeDef);
    }

    private boolean moveCheck(T skillDef, boolean up) {
        T other = up ? findNext(skillDef) : findPrev(skillDef);
        if (other == null) {
            v("could not find other for level " + skillDef.getLevel());
            return false;
        }
        int otherLevel = other.getLevel();
        other.setLevel(skillDef.getLevel());
        skillDef.setLevel(otherLevel);
        return move(skillDef, other);
    }

    private T findNext(T skillDef) {
        T other = null;
        List<T> attributes = getAll();
        for (T attrDef : attributes) {
            if (attrDef.getLevel() < skillDef.getLevel()) {
                other = attrDef;
            } else if (attrDef == skillDef) break;
        }
        return other;
    }

    private T findPrev(T skillDef) {
        List<T> attributes = getAll();
        for (T attrDef : attributes) {
            if (attrDef.getLevel() > skillDef.getLevel()) {
                return attrDef;
            }
        }
        return null;
    }

    protected abstract List<T> getAll();

    protected abstract boolean create(String name, PnPEntityAttributeDefMain attributeDef);

    protected abstract boolean update(T skillDef, String name, PnPEntityAttributeDefMain attributeDef);

    protected abstract boolean move(T skillDef, T other);

    protected abstract void delete(T skillDef);
}

package de.pnp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class PnPUtil {


    public static class PnPStrings {

        public static final String EMPTY = "";


        public static final String COLUMN_ID = "id";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_RACE = "race";
        public static final String COLUMN_ATTRIBUTE = "attribute";
        public static final String COLUMN_VALUE = "value";
        public static final String COLUMN_MIN_VALUE = "min_value";
        public static final String COLUMN_MAX_VALUE = "max_value";
        public static final String COLUMN_LEVEL = "level";
        public static final String COLUMN_METATYPE = "metatype";
        public static final String COLUMN_FIRST_NAME = "first_name";
        public static final String COLUMN_LAST_NAME = "last_name";
        public static final String COLUMN_OWNER = "owner";
        public static final String COLUMN_PRIORITY = "priority";
//        public static final String COLUMN_XP = "xp";

        public static final String DB_NAME = "pnp_orm_db";
        public static final String HOMO_SAPIENS = "Sapiens";
        public static final String HOMO_GOBLINIS = "Goblinis";
        public static final String HUMAN = "Homo";
        public static final String REACTION = "Reaction";
        public static final String A = "A";
        public static final String B = "B";
        public static final String C = "C";
        public static final String D = "D";
        public static final String E = "E";

        public static final String[] QUERY_METATYPE = {COLUMN_METATYPE};
        public static final String[] QUERY_RACE = {COLUMN_RACE};
        public static final String[] QUERY_ATTRIBUTE = {COLUMN_ATTRIBUTE};
        public static final String[] QUERY_RACE_ATTRIBUTE =
                {PnPUtil.PnPStrings.COLUMN_RACE, PnPUtil.PnPStrings.COLUMN_ATTRIBUTE};
        public static final String[] QUERY_METATYPE_ATTRIBUTE =
                {PnPUtil.PnPStrings.COLUMN_METATYPE, PnPUtil.PnPStrings.COLUMN_ATTRIBUTE};
        public static final String SPACE = " ";
    }

    public static class PnPIntegers {

        public static final int DB_VERSION = 1;
    }

    private static final TableRow.LayoutParams tableWeightLayout =
            new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 1f);
    private static final TableRow.LayoutParams buttonWeightLayout =
            new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, 1f);
    private static final TableRow.LayoutParams divLayout =
            new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 4);

    public static TableRow createTableHead(Context context, TableLayout table, String... names) {
        TableRow tr = new TableRow(context);
        tr.setBackgroundColor(context.getResources().getColor(R.color.colorBackgroud));
        for (String name : names) {
            TextView nameHead = new TextView(context);
            nameHead.setLayoutParams(tableWeightLayout);
            nameHead.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.text_heading));
            nameHead.setGravity(Gravity.CENTER);
            nameHead.setTypeface(nameHead.getTypeface(), Typeface.BOLD_ITALIC);
            nameHead.setText(name);
            tr.addView(nameHead);
        }
        table.addView(tr);
        return tr;
    }

    public static TableRow createTableRow(Context context, TableLayout table, String... names) {
        return createTableRow(context, table, null, names);
    }

    public static TableRow createTableRow(Context context, TableLayout table,
                                          TableLayout.LayoutParams layoutParams, String... names) {
        TableRow tr = new TableRow(context);
        tr.setBackgroundColor(context.getResources().getColor(R.color.colorBackgroud));
        for (String name : names) {
            TextView nameHead = new TextView(context);
            nameHead.setLayoutParams(tableWeightLayout);
            nameHead.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.text_normal));
            nameHead.setGravity(Gravity.CENTER);
            nameHead.setTypeface(nameHead.getTypeface(), Typeface.BOLD_ITALIC);
            nameHead.setText(name);
            tr.addView(nameHead);
        }
        if (layoutParams != null) {
            tr.setLayoutParams(layoutParams);
            table.addView(tr, layoutParams);
        } else {
            table.addView(tr);
        }
        return tr;
    }

    public static TableRow createTableRow(Context context, TableLayout table, View... views) {
        return createTableRow(context, table, null, views);
    }

    public static TableRow createTableRow(Context context, TableLayout table,
                                          TableLayout.LayoutParams layoutParams, View... views) {
        TableRow tr = new TableRow(context);
        tr.setBackgroundColor(context.getResources().getColor(R.color.colorBackgroud));
        for (View view : views) {
            if (view instanceof ImageButton) {
                view.setLayoutParams(buttonWeightLayout);
            } else {
                view.setLayoutParams(tableWeightLayout);
            }
            if (view instanceof TextView) {
                ((TextView) view).setTextSize(TypedValue.COMPLEX_UNIT_PX,
                        context.getResources().getDimension(R.dimen.text_normal));
                ((TextView) view).setGravity(Gravity.CENTER);
            }
            tr.addView(view);
        }
        if (layoutParams != null) {
            tr.setLayoutParams(layoutParams);
            table.addView(tr, layoutParams);
        } else {
            table.addView(tr);
        }
        return tr;
    }

    public static TableRow createTableDiv(Context context, TableLayout table,
                                          TableLayout.LayoutParams layoutParams) {
        TableRow tr = new TableRow(context);
        tr.setBackgroundColor(context.getResources().getColor(R.color.colorBackgroud));
        TextView tv = new TextView(context);
        tv.setLayoutParams(divLayout);
        tr.addView(tv);
        if (layoutParams != null) {
            tr.setLayoutParams(layoutParams);
            table.addView(tr, layoutParams);
        } else {
            table.addView(tr);
        }
        return tr;
    }

    public static TableRow createButtonRow(Context context, TableLayout table, View.OnClickListener update,
                                           View.OnClickListener delete) {
        return createButtonRow(context, table, update, delete, true);
    }

    public static TableRow createButtonRow(Context context, TableLayout table, View.OnClickListener update,
                                           View.OnClickListener delete, boolean enable) {
        ImageButton updateButton = new ImageButton(context);
        if (enable)
            updateButton.setImageResource(android.R.drawable.ic_menu_rotate);
        updateButton.setOnClickListener(update);
        updateButton.setEnabled(enable);

        ImageButton deleteButton = new ImageButton(context);
        if (enable)
            deleteButton.setImageResource(android.R.drawable.ic_delete);
        deleteButton.setOnClickListener(delete);
        deleteButton.setEnabled(enable);

        return createTableRow(context, table, updateButton, deleteButton);
    }

    public static TableRow createButtonRow(Context context, TableLayout table, View.OnClickListener update,
                                           View.OnClickListener delete, View.OnClickListener up,
                                           View.OnClickListener down) {
        ImageButton updateButton = new ImageButton(context);
        updateButton.setImageResource(android.R.drawable.ic_menu_rotate);
        updateButton.setOnClickListener(update);

        ImageButton deleteButton = new ImageButton(context);
        deleteButton.setImageResource(android.R.drawable.ic_delete);
        deleteButton.setOnClickListener(delete);

        ImageButton upButton = new ImageButton(context);
        upButton.setImageResource(android.R.drawable.arrow_up_float);
        upButton.setOnClickListener(up);

        ImageButton downButton = new ImageButton(context);
        downButton.setImageResource(android.R.drawable.arrow_down_float);
        downButton.setOnClickListener(down);

        return createTableRow(context, table, updateButton, deleteButton, upButton, downButton);
    }
}

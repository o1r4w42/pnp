package de.pnp;

import android.util.Log;

public interface PnPLogger {

    default void e(String msg){
        Log.e(getClass().getSimpleName(), msg);
    }

    default void e(String msg, Exception e){
        Log.e(getClass().getSimpleName(), msg, e);
    }

    default void v(String msg){
        Log.v(getClass().getSimpleName(), msg);
    }
}

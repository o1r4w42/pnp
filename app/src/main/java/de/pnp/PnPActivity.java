package de.pnp;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import javax.inject.Inject;

import de.pnp.database.PnPDBHandler;
import de.pnp.fragment.PnPHandlerFragment;
import de.pnp.injection.DaggerPnPInjectionComponent;
import de.pnp.injection.PnPInjectionComponent;
import de.pnp.injection.PnPInjectionModule;

public class PnPActivity extends AppCompatActivity implements PnPLogger{

    private PnPInjectionComponent injector;

    @Inject PnPDBHandler dbHandler;
    @Inject PnPHandlerFragment fragmentHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        injector = DaggerPnPInjectionComponent.builder()
                .pnPInjectionModule(new PnPInjectionModule(this))
                .build();
        injector.inject(this);
        injector.inject(dbHandler);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dbHandler != null) {
            OpenHelperManager.releaseHelper();
            dbHandler = null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        fragmentHandler.enterFragment(item.getItemId());
        return super.onOptionsItemSelected(item);
    }

    public void hideInput() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            view.clearFocus();
        }
    }

    public PnPInjectionComponent getInjector() {
        return injector;
    }
}

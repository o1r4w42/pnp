package de.pnp.injection;

import javax.inject.Singleton;

import dagger.Component;
import de.pnp.PnPActivity;
import de.pnp.database.PnPDBHandler;
import de.pnp.database.handler.PnPEntityHandlerMetaType2Attribute;
import de.pnp.database.handler.PnPEntityHandlerMetaTypeDef;
import de.pnp.database.handler.PnPEntityHandlerPriority;
import de.pnp.database.handler.PnPEntityHandlerRace2Attribute;
import de.pnp.database.handler.PnPEntityHandlerSkillActive;
import de.pnp.database.handler.PnPEntityHandlerSkillPassive;
import de.pnp.fragment.PnPFragment;
import de.pnp.fragment.sub.PnPSubFragment;
import de.pnp.fragment.sub.character.PnPSubFragmentCharAttrMain;
import de.pnp.fragment.sub.database.PnPSubFragmentDBAttrMain;
import de.pnp.fragment.sub.database.PnPSubFragmentDBAttrSub;
import de.pnp.fragment.sub.database.PnPSubFragmentDBMetaType;
import de.pnp.fragment.sub.database.PnPSubFragmentDBPriority;
import de.pnp.fragment.sub.database.PnPSubFragmentDBRace;
import de.pnp.fragment.sub.database.PnPSubFragmentDBSkillActive;
import de.pnp.fragment.sub.database.PnPSubFragmentDBSkillPassive;

@Singleton
@Component(modules = {PnPInjectionModule.class})
public interface PnPInjectionComponent {

    void inject(PnPActivity activity);

    void inject(PnPFragment fragment);

    void inject(PnPSubFragment subFragment);

    void inject(PnPDBHandler dbHandler);

    //inject entity handler
    void inject(PnPEntityHandlerMetaType2Attribute entityHandler);

    void inject(PnPEntityHandlerMetaTypeDef entityHandler);

    void inject(PnPEntityHandlerRace2Attribute entityHandler);

    void inject(PnPEntityHandlerSkillActive entityHandler);

    void inject(PnPEntityHandlerSkillPassive entityHandler);

    //inject sub fragment implementations
    void inject(PnPSubFragmentDBAttrMain subFragment);

    void inject(PnPSubFragmentDBAttrSub subFragment);

    void inject(PnPSubFragmentDBMetaType subFragment);

    void inject(PnPSubFragmentDBRace subFragment);

    void inject(PnPSubFragmentDBSkillActive subFragment);

    void inject(PnPSubFragmentDBSkillPassive subFragment);

    void inject(PnPSubFragmentCharAttrMain subFragment);

    void inject(PnPSubFragmentDBPriority subFragment);
}

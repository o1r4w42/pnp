package de.pnp.injection;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.pnp.PnPActivity;
import de.pnp.PnPAlertHandler;
import de.pnp.PnPLogger;
import de.pnp.database.PnPDBHandler;
import de.pnp.database.entity.attribute.PnPEntityAttributeDefMain;
import de.pnp.database.entity.attribute.PnPEntityAttributeDefSub;
import de.pnp.database.entity.priority.PnPEntityPriority;
import de.pnp.database.handler.PnPEntityHandlerAttributeDefMain;
import de.pnp.database.handler.PnPEntityHandlerAttributeDefSub;
import de.pnp.database.handler.PnPEntityHandlerMetaType2Attribute;
import de.pnp.database.handler.PnPEntityHandlerMetaTypeDef;
import de.pnp.database.handler.PnPEntityHandlerPriority;
import de.pnp.database.handler.PnPEntityHandlerRace2Attribute;
import de.pnp.database.handler.PnPEntityHandlerRaceDef;
import de.pnp.database.handler.PnPEntityHandlerSkillActive;
import de.pnp.database.handler.PnPEntityHandlerSkillPassive;
import de.pnp.database.entity.race.PnPEntityMetaType2Attribute;
import de.pnp.database.entity.race.PnPEntityMetaTypeDef;
import de.pnp.database.entity.race.PnPEntityRace2Attribute;
import de.pnp.database.entity.race.PnPEntityRaceDef;
import de.pnp.database.entity.skill.PnPEntitySkillDefActive;
import de.pnp.database.entity.skill.PnPEntitySkillDefPassive;
import de.pnp.fragment.PnPHandlerFragment;

@Module
public class PnPInjectionModule implements PnPLogger {

    private final PnPActivity activity;

    public PnPInjectionModule(PnPActivity activity) {
        this.activity = activity;
    }

    @Provides
    @Singleton
    PnPAlertHandler alertHandler() {
        return new PnPAlertHandler(activity);
    }

    @Provides
    @Singleton
    PnPDBHandler dbHandler() {
        PnPDBHandler dbHandler = OpenHelperManager.getHelper(activity, PnPDBHandler.class);
//        dbHandler.setActivity(activity);
        return dbHandler;
    }

    @Provides
    @Singleton
    PnPHandlerFragment fragmentHandler(){
        return new PnPHandlerFragment(activity.getSupportFragmentManager());
    }

    @Provides
    @Singleton
    PnPEntityHandlerAttributeDefMain attributeDefMainHandler(PnPDBHandler dbHandler){
        try {
            PnPEntityHandlerAttributeDefMain handler =
                    new PnPEntityHandlerAttributeDefMain(dbHandler.getDao(PnPEntityAttributeDefMain.class), activity.getInjector());
//            dbHandler.addEntityHandler(handler);
            return handler;
        } catch (SQLException e) {
            e("Unable to create PnPEntityAttributeDefMain Dao!", e);
        }
        return null;
    }

    @Provides
    @Singleton
    PnPEntityHandlerAttributeDefSub attributeDefSubHandler(PnPDBHandler dbHandler){
        try {
            PnPEntityHandlerAttributeDefSub handler = new PnPEntityHandlerAttributeDefSub(dbHandler.getDao(PnPEntityAttributeDefSub.class), activity.getInjector());
//            dbHandler.addEntityHandler(handler);
            return handler;
        } catch (SQLException e) {
            e("Unable to create PnPEntityAttributeDefSub Dao!", e);
        }
        return null;
    }

    @Provides
    @Singleton
    PnPEntityHandlerMetaType2Attribute metaType2AttributeHandler(PnPDBHandler dbHandler){
        try {
            PnPEntityHandlerMetaType2Attribute handler = new PnPEntityHandlerMetaType2Attribute(dbHandler.getDao(PnPEntityMetaType2Attribute.class), activity.getInjector());
//            dbHandler.addEntityHandler(handler);
            return handler;
        } catch (SQLException e) {
            e("Unable to create PnPEntityMetaType2Attribute Dao!", e);
        }
        return null;
    }

    @Provides
    @Singleton
    PnPEntityHandlerMetaTypeDef metaTypeDefHandler(PnPDBHandler dbHandler){
        try {
            PnPEntityHandlerMetaTypeDef handler = new PnPEntityHandlerMetaTypeDef(dbHandler.getDao(PnPEntityMetaTypeDef.class), activity.getInjector());
//            dbHandler.addEntityHandler(handler);
            return handler;
        } catch (SQLException e) {
            e("Unable to create PnPEntityMetaTypeDef Dao!", e);
        }
        return null;
    }

    @Provides
    @Singleton
    PnPEntityHandlerRace2Attribute race2AttributeHandler(PnPDBHandler dbHandler){
        try {
            PnPEntityHandlerRace2Attribute handler = new PnPEntityHandlerRace2Attribute(dbHandler.getDao(PnPEntityRace2Attribute.class), activity.getInjector());
//            dbHandler.addEntityHandler(handler);
            return handler;
        } catch (SQLException e) {
            e("Unable to create PnPEntityRace2Attribute Dao!", e);
        }
        return null;
    }

    @Provides
    @Singleton
    PnPEntityHandlerRaceDef raceDefHandler(PnPDBHandler dbHandler){
        try {
            PnPEntityHandlerRaceDef handler = new PnPEntityHandlerRaceDef(dbHandler.getDao(PnPEntityRaceDef.class), activity.getInjector());
//            dbHandler.addEntityHandler(handler);
            return handler;
        } catch (SQLException e) {
            e("Unable to create PnPEntityRaceDef Dao!", e);
        }
        return null;
    }

    @Provides
    @Singleton
    PnPEntityHandlerSkillActive skillActiveHandler(PnPDBHandler dbHandler){
        try {
            PnPEntityHandlerSkillActive handler = new PnPEntityHandlerSkillActive(dbHandler.getDao(PnPEntitySkillDefActive.class), activity.getInjector());
//            dbHandler.addEntityHandler(handler);
            return handler;
        } catch (SQLException e) {
            e("Unable to create PnPEntitySkillDefActive Dao!", e);
        }
        return null;
    }

    @Provides
    @Singleton
    PnPEntityHandlerSkillPassive skillPassiveHandler(PnPDBHandler dbHandler){
        try {
            PnPEntityHandlerSkillPassive handler = new PnPEntityHandlerSkillPassive(dbHandler.getDao(PnPEntitySkillDefPassive.class), activity.getInjector());
//            dbHandler.addEntityHandler(handler);
            return handler;
        } catch (SQLException e) {
            e("Unable to create PnPEntitySkillDefPassive Dao!", e);
        }
        return null;
    }

    @Provides
    @Singleton
    PnPEntityHandlerPriority priorityHandler(PnPDBHandler dbHandler){
        try {
            PnPEntityHandlerPriority handler = new PnPEntityHandlerPriority(dbHandler.getDao(PnPEntityPriority.class), activity.getInjector());
            return handler;
        } catch (SQLException e) {
            e("Unable to create PnPEntityPriority Dao!", e);
        }
        return null;
    }
}

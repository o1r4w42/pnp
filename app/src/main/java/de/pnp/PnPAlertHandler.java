package de.pnp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;

public class PnPAlertHandler {

    private final PnPActivity activity;
    private final AlertDialog.Builder builder;
    private final AlertDialog confirmAlert;
    private final AlertDialog infoAlert;
    private final AlertDialog errorAlert;
    private final PnPAlterClickListener okListener;
    private final PnPAlterClickListener noListener;

    public PnPAlertHandler(PnPActivity activity){
        this.activity = activity;
        builder = new AlertDialog.Builder(activity);

        confirmAlert = builder.create();
        infoAlert = builder.create();
        errorAlert = builder.create();

        okListener = new PnPAlterClickListener();
        noListener = new PnPAlterClickListener();
    }

    public void info(String title, String text) {
        infoAlert.setTitle(title);
        infoAlert.setMessage(text);
        infoAlert.show();
    }

    public void confirm(String title, String text, View.OnClickListener ok) {
        confirm(title, text, ok, null);
    }

    public void confirm(String title, String text, View.OnClickListener ok, View.OnClickListener no) {
        confirmAlert.setTitle(title);
        confirmAlert.setMessage(text);
        okListener.setSubListener(ok);
        noListener.setSubListener(no);
        confirmAlert.setButton(AlertDialog.BUTTON_POSITIVE, activity.getString(R.string.ok), okListener);
        confirmAlert.setButton(AlertDialog.BUTTON_NEGATIVE, activity.getString(R.string.no), noListener);
        confirmAlert.show();
    }

    public void error(String title, String text) {
        errorAlert.setTitle(title);
        errorAlert.setMessage(text);
        errorAlert.show();
    }

    private class PnPAlterClickListener implements DialogInterface.OnClickListener{

        private View.OnClickListener subListener;

        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            if(subListener != null)
                subListener.onClick(null);
        }

        public void setSubListener(View.OnClickListener subListener) {
            this.subListener = subListener;
        }
    }
}

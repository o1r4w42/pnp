package de.pnp.database;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

import java.io.IOException;
import java.sql.SQLException;

import de.pnp.database.entity.attribute.PnPEntityAttributeDefMain;
import de.pnp.database.entity.attribute.PnPEntityAttributeDefSub;
import de.pnp.database.entity.character.PnPCharacter;
import de.pnp.database.entity.priority.PnPEntityMetaType2Priority;
import de.pnp.database.entity.priority.PnPEntityPriority;
import de.pnp.database.entity.race.PnPEntityMetaType2Attribute;
import de.pnp.database.entity.race.PnPEntityMetaTypeDef;
import de.pnp.database.entity.race.PnPEntityRace2Attribute;
import de.pnp.database.entity.race.PnPEntityRaceDef;
import de.pnp.database.entity.skill.PnPEntitySkillDefActive;
import de.pnp.database.entity.skill.PnPEntitySkillDefPassive;

public class PnPDBConfig extends OrmLiteConfigUtil {

    public static final Class<?>[] CLASSES = new Class[] {
            PnPEntityPriority.class,
            PnPEntityAttributeDefMain.class,
            PnPEntityAttributeDefSub.class,
            PnPEntitySkillDefActive.class,
            PnPEntitySkillDefPassive.class,
            PnPEntityRaceDef.class,
            PnPEntityRace2Attribute.class,
            PnPEntityMetaTypeDef.class,
            PnPEntityMetaType2Attribute.class,
            PnPEntityMetaType2Priority.class,
            PnPCharacter.class,
    };

    public static void main(String[] args) throws SQLException, IOException {
        writeConfigFile("ormlite_config.txt", CLASSES);
    }
}

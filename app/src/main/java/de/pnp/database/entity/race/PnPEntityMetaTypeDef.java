package de.pnp.database.entity.race;

import com.j256.ormlite.field.DatabaseField;

import de.pnp.PnPUtil;
import de.pnp.database.entity.PnPEntity;
import de.pnp.database.entity.PnPEntityNamed;

public class PnPEntityMetaTypeDef extends PnPEntityNamed {

    @DatabaseField(foreign = true, foreignAutoRefresh = true,
            columnName = PnPUtil.PnPStrings.COLUMN_RACE)
    private PnPEntityRaceDef raceDef;

    public PnPEntityMetaTypeDef() {
    }

    public PnPEntityMetaTypeDef(String name, PnPEntityRaceDef raceDef) {
        super(name);
        this.raceDef = raceDef;
    }

    @Override
    public String toString() {
        return raceDef.getName()+ PnPUtil.PnPStrings.SPACE+super.toString();
    }

    public PnPEntityRaceDef getRaceDef() {
        return raceDef;
    }

    public void setRaceDef(PnPEntityRaceDef raceDef) {
        this.raceDef = raceDef;
    }

    public boolean equals(String name, PnPEntityRaceDef raceDef) {
        return getName().equals(name) && this.raceDef.equals(raceDef);
    }
}

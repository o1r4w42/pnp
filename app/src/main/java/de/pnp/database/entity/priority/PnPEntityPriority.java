package de.pnp.database.entity.priority;

import de.pnp.database.entity.PnPEntityNamed;
import de.pnp.database.entity.PnPEntityOrdered;

public class PnPEntityPriority extends PnPEntityOrdered {

    public PnPEntityPriority() {
    }

    public PnPEntityPriority(String name, int level) {
        super(name, level);
    }
}

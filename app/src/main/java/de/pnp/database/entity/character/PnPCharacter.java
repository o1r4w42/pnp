package de.pnp.database.entity.character;

import com.j256.ormlite.field.DatabaseField;

import de.pnp.PnPUtil;
import de.pnp.database.entity.PnPEntity;

public class PnPCharacter extends PnPEntity{

    @DatabaseField(columnName = PnPUtil.PnPStrings.COLUMN_OWNER)
    public String owner;

    @DatabaseField(columnName = PnPUtil.PnPStrings.COLUMN_FIRST_NAME)
    public String firstName;

    @DatabaseField(columnName = PnPUtil.PnPStrings.COLUMN_LAST_NAME)
    public String lastName;

    public PnPCharacter() {
        super();
    }

    public PnPCharacter(String owner) {
        super();
        this.owner = owner;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}

package de.pnp.database.entity.skill;

import de.pnp.database.entity.attribute.PnPEntityAttributeDefMain;

public class PnPEntitySkillDefPassive extends PnPEntitySkillDef {

    private static final long serialVersionUID = 2213724490753111383L;

    public PnPEntitySkillDefPassive() {
        super();
    }

    public PnPEntitySkillDefPassive(String name, int level, PnPEntityAttributeDefMain attribute) {
        super(name, level, attribute);
    }
}

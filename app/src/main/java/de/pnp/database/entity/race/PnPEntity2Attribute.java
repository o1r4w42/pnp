package de.pnp.database.entity.race;

import com.j256.ormlite.field.DatabaseField;

import de.pnp.PnPUtil;
import de.pnp.database.entity.PnPEntity;
import de.pnp.database.entity.attribute.PnPEntityAttributeDefMain;

public abstract class PnPEntity2Attribute extends PnPEntity {

    @DatabaseField(foreign = true, foreignAutoRefresh = true,
            columnName = PnPUtil.PnPStrings.COLUMN_ATTRIBUTE)
    private PnPEntityAttributeDefMain attributeDef;

    @DatabaseField(columnName = PnPUtil.PnPStrings.COLUMN_MIN_VALUE)
    private int minValue;

    @DatabaseField(columnName = PnPUtil.PnPStrings.COLUMN_MAX_VALUE)
    private int maxValue;

    public PnPEntity2Attribute() {
        super();
    }

    public PnPEntity2Attribute(PnPEntityAttributeDefMain attributeDef, int minValue, int maxValue) {
        super();
        this.attributeDef = attributeDef;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    public PnPEntityAttributeDefMain getAttributeDef() {
        return attributeDef;
    }

    public void setAttributeDef(PnPEntityAttributeDefMain attributeDef) {
        this.attributeDef = attributeDef;
    }

    public int getMinValue() {
        return minValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }
}

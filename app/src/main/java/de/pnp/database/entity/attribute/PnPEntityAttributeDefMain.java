package de.pnp.database.entity.attribute;

public class PnPEntityAttributeDefMain extends PnPEntityAttributeDef {

    private static final long serialVersionUID = -2898010978695568211L;

    public PnPEntityAttributeDefMain() {
        super();
    }

    public PnPEntityAttributeDefMain(String name, int level) {
        super(name, level);
    }
}

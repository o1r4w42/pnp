package de.pnp.database.entity;

import com.j256.ormlite.field.DatabaseField;

import de.pnp.PnPUtil;

public abstract class PnPEntityNamed extends PnPEntity {

    @DatabaseField(columnName = PnPUtil.PnPStrings.COLUMN_NAME)
    public String name;

    public PnPEntityNamed() {
        super();
    }

    public PnPEntityNamed(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean equals(String name) {
        return getName().equals(name);
    }
}

package de.pnp.database.entity;

import java.io.Serializable;
import com.j256.ormlite.field.DatabaseField;

import de.pnp.PnPUtil;

public abstract class PnPEntity implements Serializable {

    @DatabaseField(generatedId = true, columnName = PnPUtil.PnPStrings.COLUMN_ID)
    public int id;


    public PnPEntity(){
        super();
    }


    @Override
    public boolean equals(Object other) {
        if(other instanceof PnPEntity){
            return ((PnPEntity)other).getId()==id;
        }
        return super.equals(other);
    }

    public int getId() {
        return id;
    }

}

package de.pnp.database.entity.attribute;

public class PnPEntityAttributeDefSub extends PnPEntityAttributeDef {

    private static final long serialVersionUID = -2463861222613206499L;

    public PnPEntityAttributeDefSub() {
        super();
    }

    public PnPEntityAttributeDefSub(String name, int level) {
        super(name, level);
    }
}

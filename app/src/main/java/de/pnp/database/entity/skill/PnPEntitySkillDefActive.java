package de.pnp.database.entity.skill;

import de.pnp.database.entity.attribute.PnPEntityAttributeDefMain;

public class PnPEntitySkillDefActive extends PnPEntitySkillDef {

    private static final long serialVersionUID = -625894661145467880L;

    public PnPEntitySkillDefActive() {
        super();
    }

    public PnPEntitySkillDefActive(String name, int level, PnPEntityAttributeDefMain attribute) {
        super(name, level, attribute);
    }
}

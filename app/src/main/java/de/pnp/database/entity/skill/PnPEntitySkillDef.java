package de.pnp.database.entity.skill;

import com.j256.ormlite.field.DatabaseField;

import de.pnp.PnPUtil;
import de.pnp.database.entity.PnPEntityOrdered;
import de.pnp.database.entity.attribute.PnPEntityAttributeDefMain;

public abstract class PnPEntitySkillDef extends PnPEntityOrdered {

    @DatabaseField(foreign = true, foreignAutoRefresh = true,
            columnName = PnPUtil.PnPStrings.COLUMN_ATTRIBUTE)
    private PnPEntityAttributeDefMain attribute;

    public PnPEntitySkillDef() {
        super();
    }

    public PnPEntitySkillDef(String name, int level, PnPEntityAttributeDefMain attribute) {
        super(name, level);
        this.attribute = attribute;
    }

    public PnPEntityAttributeDefMain getAttribute() {
        return attribute;
    }

    public void setAttribute(PnPEntityAttributeDefMain attribute) {
        this.attribute = attribute;
    }

    public boolean equals(String name, PnPEntityAttributeDefMain attributeDef) {
        return getName().equals(name) && getAttribute().equals(attributeDef);
    }
}

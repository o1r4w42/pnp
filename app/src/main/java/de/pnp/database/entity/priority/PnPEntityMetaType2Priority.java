package de.pnp.database.entity.priority;

import com.j256.ormlite.field.DatabaseField;

import de.pnp.PnPUtil;
import de.pnp.database.entity.PnPEntityOrdered;
import de.pnp.database.entity.race.PnPEntityMetaTypeDef;

public class PnPEntityMetaType2Priority extends PnPEntityOrdered {

    @DatabaseField(foreign = true, foreignAutoRefresh = true,
            columnName = PnPUtil.PnPStrings.COLUMN_METATYPE)
    private PnPEntityMetaTypeDef metaDef;

    @DatabaseField(foreign = true, foreignAutoRefresh = true,
            columnName = PnPUtil.PnPStrings.COLUMN_PRIORITY)
    private PnPEntityPriority priority;

    @DatabaseField(columnName = PnPUtil.PnPStrings.COLUMN_VALUE)
    private int value;

    public PnPEntityMetaType2Priority() {
        super();
    }

    public PnPEntityMetaType2Priority(String name, int level, PnPEntityMetaTypeDef metaDef,
                                      PnPEntityPriority priority, int value) {
        super(name, level);
        this.metaDef = metaDef;
        this.priority = priority;
        this.value = value;
    }

    public PnPEntityMetaTypeDef getMetaDef() {
        return metaDef;
    }

    public void setMetaDef(PnPEntityMetaTypeDef metaDef) {
        this.metaDef = metaDef;
    }

    public PnPEntityPriority getPriority() {
        return priority;
    }

    public void setPriority(PnPEntityPriority priority) {
        this.priority = priority;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}

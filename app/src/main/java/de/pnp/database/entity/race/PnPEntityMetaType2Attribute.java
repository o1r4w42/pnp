package de.pnp.database.entity.race;

import com.j256.ormlite.field.DatabaseField;

import de.pnp.PnPUtil;
import de.pnp.database.entity.PnPEntity;
import de.pnp.database.entity.attribute.PnPEntityAttributeDefMain;

public class PnPEntityMetaType2Attribute extends PnPEntity2Attribute {

    @DatabaseField(foreign = true, foreignAutoRefresh = true,
            columnName = PnPUtil.PnPStrings.COLUMN_METATYPE)
    private PnPEntityMetaTypeDef metaDef;

    public PnPEntityMetaType2Attribute() {
        super();
    }

    public PnPEntityMetaType2Attribute(PnPEntityAttributeDefMain attributeDef, int minValue, int maxValue,
                                       PnPEntityMetaTypeDef metaDef) {
        super(attributeDef, minValue, maxValue);
        this.metaDef = metaDef;
    }

    public PnPEntityMetaTypeDef getMetaDef() {
        return metaDef;
    }

    public void setMetaDef(PnPEntityMetaTypeDef metaDef) {
        this.metaDef = metaDef;
    }

    public boolean equals(PnPEntityAttributeDefMain attributeDef, int minValue, int maxValue,
                          PnPEntityMetaTypeDef metaDef) {
        return this.metaDef.equals(metaDef) && getAttributeDef().equals(attributeDef) &&
                getMinValue() == minValue && getMaxValue() == maxValue;
    }
}

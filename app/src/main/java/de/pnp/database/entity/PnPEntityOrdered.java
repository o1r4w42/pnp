package de.pnp.database.entity;

import android.support.annotation.NonNull;

import com.j256.ormlite.field.DatabaseField;

import de.pnp.PnPUtil;

public abstract class PnPEntityOrdered extends PnPEntityNamed implements Comparable<PnPEntityOrdered> {

    @DatabaseField(columnName = PnPUtil.PnPStrings.COLUMN_LEVEL)
    private int level;

    public PnPEntityOrdered() {
        super();
    }

    public PnPEntityOrdered(String name, int level) {
        super(name);
        this.level = level;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public int compareTo(@NonNull PnPEntityOrdered other) {
        if (level > other.level) return 1;
        if (level < other.level) return -1;
        return 0;
    }
}

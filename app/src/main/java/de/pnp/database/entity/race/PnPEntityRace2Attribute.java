package de.pnp.database.entity.race;

import com.j256.ormlite.field.DatabaseField;

import de.pnp.PnPUtil;
import de.pnp.database.entity.attribute.PnPEntityAttributeDefMain;

public class PnPEntityRace2Attribute extends PnPEntity2Attribute {

    @DatabaseField(foreign = true, foreignAutoRefresh = true,
            columnName = PnPUtil.PnPStrings.COLUMN_RACE)
    private PnPEntityRaceDef raceDef;

    public PnPEntityRace2Attribute() {
        super();
    }

    public PnPEntityRace2Attribute(PnPEntityAttributeDefMain attributeDef, int minValue, int maxValue,
                                   PnPEntityRaceDef raceDef) {
        super(attributeDef, minValue, maxValue);
        this.raceDef = raceDef;
    }

    public PnPEntityRaceDef getRaceDef() {
        return raceDef;
    }

    public void setRaceDef(PnPEntityRaceDef raceDef) {
        this.raceDef = raceDef;
    }

    public boolean equals(PnPEntityAttributeDefMain attributeDef, int minValue, int maxValue,
                          PnPEntityRaceDef raceDef) {
        return this.raceDef.equals(raceDef) && getAttributeDef().equals(attributeDef) &&
                getMinValue() == minValue && getMaxValue() == maxValue;
    }
}

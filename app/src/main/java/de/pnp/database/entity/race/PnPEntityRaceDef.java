package de.pnp.database.entity.race;

import de.pnp.database.entity.PnPEntity;
import de.pnp.database.entity.PnPEntityNamed;

public class PnPEntityRaceDef extends PnPEntityNamed {

    public PnPEntityRaceDef() {
        super();
    }

    public PnPEntityRaceDef(String name) {
        super(name);
    }

    public boolean equals(String name) {
        return getName().equals(name);
    }
}

package de.pnp.database.entity.attribute;

import de.pnp.database.entity.PnPEntityOrdered;

public abstract class PnPEntityAttributeDef extends PnPEntityOrdered {

    public PnPEntityAttributeDef() {
        super();
    }

    public PnPEntityAttributeDef(String name, int level) {
        super(name, level);
    }



}

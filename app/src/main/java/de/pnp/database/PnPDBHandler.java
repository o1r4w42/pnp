package de.pnp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import javax.inject.Inject;

import de.pnp.PnPLogger;
import de.pnp.PnPUtil;
import de.pnp.database.handler.PnPEntityHandlerMetaType2Attribute;
import de.pnp.database.handler.PnPEntityHandlerMetaTypeDef;
import de.pnp.database.handler.PnPEntityHandlerPriority;
import de.pnp.database.handler.PnPEntityHandlerRace2Attribute;
import de.pnp.database.handler.PnPEntityHandlerRaceDef;
import de.pnp.database.handler.PnPEntityHandlerSkillPassive;
import de.pnp.database.handler.PnPEntityHandlerSkillActive;
import de.pnp.database.handler.PnPEntityHandlerAttributeDefMain;
import de.pnp.database.handler.PnPEntityHandlerAttributeDefSub;

public class PnPDBHandler extends OrmLiteSqliteOpenHelper implements PnPLogger {

    @Inject
    PnPEntityHandlerPriority priorityHandler;
    @Inject
    PnPEntityHandlerAttributeDefMain attributeDefMainHandler;
    @Inject
    PnPEntityHandlerAttributeDefSub attributeDefSubHandler;
    @Inject
    PnPEntityHandlerRaceDef raceDefHandler;
    @Inject
    PnPEntityHandlerRace2Attribute race2AttributeHandler;
    @Inject
    PnPEntityHandlerMetaTypeDef metaTypeDefHandler;
    @Inject
    PnPEntityHandlerMetaType2Attribute metaType2AttributeHandler;
    @Inject
    PnPEntityHandlerSkillActive skillActiveHandler;
    @Inject
    PnPEntityHandlerSkillPassive skillPassiveHandler;

    public PnPDBHandler(Context context) {
        super(context, PnPUtil.PnPStrings.DB_NAME, null, PnPUtil.PnPIntegers.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            for (Class<?> clazz : PnPDBConfig.CLASSES) {
                TableUtils.createTable(getConnectionSource(), clazz);
            }
            init();
        } catch (SQLException e) {
            e("Unable to create database!", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        try {
            for (Class<?> clazz : PnPDBConfig.CLASSES) {
                TableUtils.dropTable(getConnectionSource(), clazz, true);
            }
            onCreate(sqLiteDatabase, connectionSource);
        } catch (SQLException e) {
            e("Unable to update database from " + oldVersion + " to " + newVersion + "!", e);
        }
    }

    public boolean initDefaults() {
        try {
            for (Class<?> clazz : PnPDBConfig.CLASSES) {
                TableUtils.clearTable(getConnectionSource(), clazz);
            }
            init();
            return true;
        } catch (SQLException e) {
            e("Unable to clear Tables!", e);
            return false;
        }
    }

    private void init() {
        priorityHandler.createDefaults();
        attributeDefMainHandler.createDefaults();
        attributeDefSubHandler.createDefaults();
        raceDefHandler.createDefaults();
        race2AttributeHandler.createDefaults();
        metaTypeDefHandler.createDefaults();
        metaType2AttributeHandler.createDefaults();
        skillActiveHandler.createDefaults();
        skillPassiveHandler.createDefaults();
    }
}
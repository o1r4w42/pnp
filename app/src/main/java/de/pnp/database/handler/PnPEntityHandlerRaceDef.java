package de.pnp.database.handler;

import com.j256.ormlite.dao.Dao;

import de.pnp.PnPUtil;
import de.pnp.database.entity.race.PnPEntityRaceDef;
import de.pnp.injection.PnPInjectionComponent;

public class PnPEntityHandlerRaceDef extends PnPEntityHandler<PnPEntityRaceDef> {

    public PnPEntityHandlerRaceDef(Dao<PnPEntityRaceDef, Integer> dao, PnPInjectionComponent injector) {
        super(dao, injector);
    }

    @Override
    public void createDefaults() {
        create(PnPUtil.PnPStrings.HUMAN);
    }

    public boolean create(String name) {
        return create(new PnPEntityRaceDef(name));
    }
}

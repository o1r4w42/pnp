package de.pnp.database.handler;

import com.j256.ormlite.dao.Dao;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import de.pnp.database.entity.attribute.PnPEntityAttributeDefMain;
import de.pnp.database.entity.skill.PnPEntitySkillDefActive;
import de.pnp.injection.PnPInjectionComponent;

public class PnPEntityHandlerSkillActive extends PnPEntityHandler<PnPEntitySkillDefActive> {

    @Inject
    PnPEntityHandlerAttributeDefMain mainAttributeDefHandler;

    public PnPEntityHandlerSkillActive(Dao<PnPEntitySkillDefActive, Integer> dao, PnPInjectionComponent injector) {
        super(dao, injector);
        injector.inject(this);
    }

    @Override
    protected List<PnPEntitySkillDefActive> sort(List<PnPEntitySkillDefActive> entities) {
        Collections.sort(entities);
        return super.sort(entities);
    }

    public void createDefaults() {
        create("Computer", mainAttributeDefHandler.getEntity("Logic"));
        create("Unarmed Combat", mainAttributeDefHandler.getEntity("Strength"));
    }

    public boolean create(String name, PnPEntityAttributeDefMain attributeDef) {
        int level = 0;
        List<PnPEntitySkillDefActive> attributes = getAll();
        if (attributes.size() > 0) {
            level = attributes.get(attributes.size() - 1).getLevel() + 1;
        }
        return create(new PnPEntitySkillDefActive(name, level, attributeDef));
    }
}

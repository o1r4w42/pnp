package de.pnp.database.handler;

import com.j256.ormlite.dao.Dao;

import java.util.Collections;
import java.util.List;

import de.pnp.PnPUtil;
import de.pnp.database.entity.priority.PnPEntityPriority;
import de.pnp.injection.PnPInjectionComponent;

public class PnPEntityHandlerPriority extends PnPEntityHandler<PnPEntityPriority> {

    public PnPEntityHandlerPriority(Dao<PnPEntityPriority, Integer> dao, PnPInjectionComponent injector) {
        super(dao, injector);
    }

    @Override
    protected List<PnPEntityPriority> sort(List<PnPEntityPriority> entities) {
        Collections.sort(entities);
        return super.sort(entities);
    }

    @Override
    public void createDefaults() {
        create(PnPUtil.PnPStrings.A);
        create(PnPUtil.PnPStrings.B);
        create(PnPUtil.PnPStrings.C);
        create(PnPUtil.PnPStrings.D);
        create(PnPUtil.PnPStrings.E);
    }

    public boolean create(String name) {
        int level = 0;
        List<PnPEntityPriority> priorities = getAll();
        if (priorities.size() > 0) {
            level = priorities.get(priorities.size() - 1).getLevel() + 1;
        }
        return create(new PnPEntityPriority(name, level));
    }
}

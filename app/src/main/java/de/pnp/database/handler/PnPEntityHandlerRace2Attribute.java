package de.pnp.database.handler;

import com.j256.ormlite.dao.Dao;

import java.util.List;

import javax.inject.Inject;

import de.pnp.PnPUtil;
import de.pnp.database.entity.attribute.PnPEntityAttributeDefMain;
import de.pnp.database.entity.race.PnPEntityRace2Attribute;
import de.pnp.database.entity.race.PnPEntityRaceDef;
import de.pnp.injection.PnPInjectionComponent;

import static de.pnp.PnPUtil.PnPStrings.QUERY_RACE_ATTRIBUTE;
import static de.pnp.PnPUtil.PnPStrings.QUERY_ATTRIBUTE;
import static de.pnp.PnPUtil.PnPStrings.QUERY_RACE;

public class PnPEntityHandlerRace2Attribute extends PnPEntityHandler<PnPEntityRace2Attribute> {


    @Inject
    PnPEntityHandlerAttributeDefMain attributeDefHandler;

    @Inject
    PnPEntityHandlerRaceDef raceDefHandler;

    public PnPEntityHandlerRace2Attribute(Dao<PnPEntityRace2Attribute, Integer> dao, PnPInjectionComponent injector) {
        super(dao, injector);
        injector.inject(this);
    }

    @Override
    public void createDefaults() {
        PnPEntityRaceDef human = raceDefHandler.getEntity(PnPUtil.PnPStrings.HUMAN);
        List<PnPEntityAttributeDefMain> attrs = attributeDefHandler.getAll();
        for (PnPEntityAttributeDefMain attr : attrs) {
            create(human, attr, 1, 6);
        }
    }

    private boolean create(PnPEntityRaceDef raceDef, PnPEntityAttributeDefMain attributeDef,
                           int minValue, int maxValue) {
        return create(new PnPEntityRace2Attribute(attributeDef, minValue, maxValue,raceDef));
    }

    public List<PnPEntityRace2Attribute> getAll(PnPEntityRaceDef raceDef) {
        return queryAll(QUERY_RACE, raceDef.getId());
    }

    public List<PnPEntityRace2Attribute> getAll(PnPEntityAttributeDefMain attributeDef) {
        return queryAll(QUERY_ATTRIBUTE, attributeDef.getId());
    }

    public PnPEntityRace2Attribute getEntity(PnPEntityRaceDef raceDef,
                                             PnPEntityAttributeDefMain attributeDef) {
        return queryOne(QUERY_RACE_ATTRIBUTE, raceDef.getId(), attributeDef.getId());
    }
}

package de.pnp.database.handler;

import com.j256.ormlite.dao.Dao;

import java.util.List;

import javax.inject.Inject;

import de.pnp.PnPUtil;
import de.pnp.database.entity.attribute.PnPEntityAttributeDefMain;
import de.pnp.database.entity.race.PnPEntityMetaType2Attribute;
import de.pnp.database.entity.race.PnPEntityMetaTypeDef;
import de.pnp.injection.PnPInjectionComponent;

import static de.pnp.PnPUtil.PnPStrings.QUERY_ATTRIBUTE;
import static de.pnp.PnPUtil.PnPStrings.QUERY_METATYPE;
import static de.pnp.PnPUtil.PnPStrings.QUERY_METATYPE_ATTRIBUTE;

public class PnPEntityHandlerMetaType2Attribute extends PnPEntityHandler<PnPEntityMetaType2Attribute> {

    @Inject
    PnPEntityHandlerAttributeDefMain attributeDefHandler;
    @Inject
    PnPEntityHandlerMetaTypeDef metaDefHandler;

    public PnPEntityHandlerMetaType2Attribute(Dao<PnPEntityMetaType2Attribute, Integer> dao, PnPInjectionComponent injector) {
        super(dao, injector);
        injector.inject(this);
    }

    @Override
    public void createDefaults() {
        PnPEntityMetaTypeDef gobbo = metaDefHandler.getEntity(PnPUtil.PnPStrings.HOMO_GOBLINIS);
        PnPEntityAttributeDefMain reaction = attributeDefHandler.getEntity(PnPUtil.PnPStrings.REACTION);
        create(gobbo, reaction, 2, 7);
    }

    public boolean create(PnPEntityMetaTypeDef metaTypeDef, PnPEntityAttributeDefMain attributeDef,
                          int minValue, int maxValue) {
        return create(new PnPEntityMetaType2Attribute(attributeDef, minValue, maxValue, metaTypeDef));
    }


    public PnPEntityMetaType2Attribute getEntity(PnPEntityMetaTypeDef metaTypeDef,
                                                 PnPEntityAttributeDefMain attributeDef) {
        return queryOne(QUERY_METATYPE_ATTRIBUTE, metaTypeDef.getId(), attributeDef.getId());
    }

    public List<PnPEntityMetaType2Attribute> getAll(PnPEntityAttributeDefMain attributeDef) {
        return queryAll(QUERY_ATTRIBUTE, attributeDef.getId());
    }

    public List<PnPEntityMetaType2Attribute> getEntity(PnPEntityMetaTypeDef metaDef) {
        return queryAll(QUERY_METATYPE, metaDef.getId());
    }
}

package de.pnp.database.handler;

import com.j256.ormlite.dao.Dao;

import javax.inject.Inject;

import de.pnp.PnPUtil;
import de.pnp.database.entity.race.PnPEntityMetaTypeDef;
import de.pnp.database.entity.race.PnPEntityRaceDef;
import de.pnp.injection.PnPInjectionComponent;

import static de.pnp.PnPUtil.PnPStrings.HUMAN;

public class PnPEntityHandlerMetaTypeDef extends PnPEntityHandler<PnPEntityMetaTypeDef> {

    @Inject PnPEntityHandlerRaceDef raceDefHandler;


    public PnPEntityHandlerMetaTypeDef(Dao<PnPEntityMetaTypeDef, Integer> dao, PnPInjectionComponent injector) {
        super(dao, injector);
        injector.inject(this);
    }

    @Override
    public void createDefaults() {
        PnPEntityRaceDef human = raceDefHandler.getEntity(HUMAN);
        create(PnPUtil.PnPStrings.HOMO_SAPIENS, human);
        create(PnPUtil.PnPStrings.HOMO_GOBLINIS, human);
    }

    public boolean create(String name, PnPEntityRaceDef raceDef) {
        return create(new PnPEntityMetaTypeDef(name, raceDef));
    }

}

package de.pnp.database.handler;

import com.j256.ormlite.dao.Dao;

import java.util.Collections;
import java.util.List;

import de.pnp.database.entity.attribute.PnPEntityAttributeDefSub;
import de.pnp.injection.PnPInjectionComponent;

public class PnPEntityHandlerAttributeDefSub extends PnPEntityHandler<PnPEntityAttributeDefSub> {

    public PnPEntityHandlerAttributeDefSub(Dao<PnPEntityAttributeDefSub, Integer> dao, PnPInjectionComponent injector) {
        super(dao, injector);
    }

    @Override
    protected List<PnPEntityAttributeDefSub> sort(List<PnPEntityAttributeDefSub> entities) {
        Collections.sort(entities);
        return super.sort(entities);
    }

    public void createDefaults() {
        create("Edge");
        create("Initiative");
        create("Matrix Ini");
        create("Astral Ini");
        create("Essence");
        create("Magic");
        create("Composure");
        create("Judge");
        create("Memory");
        create("Lift");
        create("Carry");
        create("Movement");
        create("Physical Limit");
        create("Mental Limit");
        create("Social Limit");
    }

    public boolean create(String name) {
        int level = 0;
        List<PnPEntityAttributeDefSub> attributes = getAll();
        if (attributes.size() > 0) {
            level = attributes.get(attributes.size() - 1).getLevel() + 1;
        }
        return create(new PnPEntityAttributeDefSub(name, level));
    }
}

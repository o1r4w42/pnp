package de.pnp.database.handler;

import com.j256.ormlite.dao.Dao;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import de.pnp.database.entity.attribute.PnPEntityAttributeDefMain;
import de.pnp.database.entity.skill.PnPEntitySkillDefPassive;
import de.pnp.injection.PnPInjectionComponent;

public class PnPEntityHandlerSkillPassive extends PnPEntityHandler<PnPEntitySkillDefPassive> {

    @Inject
    PnPEntityHandlerAttributeDefMain mainAttributeDefHandler;

    public PnPEntityHandlerSkillPassive(Dao<PnPEntitySkillDefPassive, Integer> dao, PnPInjectionComponent injector) {
        super(dao, injector);
        injector.inject(this);
    }

    @Override
    protected List<PnPEntitySkillDefPassive> sort(List<PnPEntitySkillDefPassive> entities) {
        Collections.sort(entities);
        return super.sort(entities);
    }

    public void createDefaults() {
        create("English", mainAttributeDefHandler.getEntity("Logic"));
    }

    public boolean create(String name, PnPEntityAttributeDefMain attributeDef) {
        int level = 0;
        List<PnPEntitySkillDefPassive> attributes = getAll();
        if (attributes.size() > 0) {
            level = attributes.get(attributes.size() - 1).getLevel() + 1;
        }
        return create(new PnPEntitySkillDefPassive(name, level, attributeDef));
    }
}

package de.pnp.database.handler;

import com.j256.ormlite.dao.Dao;

import java.util.Collections;
import java.util.List;

import de.pnp.PnPLogger;
import de.pnp.PnPUtil;
import de.pnp.database.entity.attribute.PnPEntityAttributeDefMain;
import de.pnp.injection.PnPInjectionComponent;

public class PnPEntityHandlerAttributeDefMain extends PnPEntityHandler<PnPEntityAttributeDefMain> implements PnPLogger {

    public PnPEntityHandlerAttributeDefMain(Dao<PnPEntityAttributeDefMain, Integer> dao, PnPInjectionComponent injector) {
        super(dao, injector);
    }

    @Override
    protected List<PnPEntityAttributeDefMain> sort(List<PnPEntityAttributeDefMain> entities) {
        Collections.sort(entities);
        return super.sort(entities);
    }

    public void createDefaults() {
        create("Body");
        create("Agility");
        create(PnPUtil.PnPStrings.REACTION);
        create("Strength");
        create("Willpower");
        create("Logic");
        create("Intuition");
        create("Charisma");
    }

    public boolean create(String name) {
        int level = 0;
        List<PnPEntityAttributeDefMain> attributes = getAll();
        if (attributes.size() > 0) {
            level = attributes.get(attributes.size() - 1).getLevel() + 1;
        }
        return create(new PnPEntityAttributeDefMain(name, level));
    }
}

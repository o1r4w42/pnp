package de.pnp.database.handler;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.util.List;

import de.pnp.PnPLogger;
import de.pnp.PnPUtil;
import de.pnp.database.entity.PnPEntity;
import de.pnp.injection.PnPInjectionComponent;

public abstract class PnPEntityHandler<T extends PnPEntity> implements PnPLogger {

    protected final Dao<T, Integer> dao;
    protected final PnPInjectionComponent injector;

    public PnPEntityHandler(Dao<T, Integer> dao, PnPInjectionComponent injector) {
        this.dao = dao;
        this.injector = injector;
    }

    public List<T> getAll() {
        try {
            List<T> entities = dao.queryForAll();
            return sort(entities);
        } catch (SQLException e) {
            e("Unable to query!", e);
        }
        return null;
    }

    protected List<T> sort(List<T> entities) {
        return entities;
    }

    public boolean update(T entity) {
        try {
            dao.update(entity);
            return true;
        } catch (SQLException e) {
            e("Unable to update!", e);
            return false;
        }
    }

    public boolean delete(T entity) {
        try {
            dao.delete(entity);
            return true;
        } catch (SQLException e) {
            e("Unable to delete!", e);
            return false;
        }
    }

    public boolean create(T entity) {
        try {
            dao.create(entity);
            return true;
        } catch (SQLException e) {
            e("Unable to create!", e);
            return false;
        }
    }

    public List<T> queryAll(String[] names, Object... values) {
        if (names.length != values.length) return null;
        try {
            QueryBuilder<T, Integer> queryBuilder = dao.queryBuilder();
            Where<T, Integer> where = queryBuilder.where();
            for(int i=0;i<names.length;i++){
                where.eq(names[i], values[i]);
                if(i<names.length-1)where.and();
            }
            PreparedQuery<T> preparedQuery = queryBuilder.prepare();
            return dao.query(preparedQuery);
        } catch (SQLException e) {
            e("Unable to query!", e);
            return null;
        }
    }

    public T queryOne(String[] names, Object... values) {
        if (names.length != values.length) return null;
        try {
            QueryBuilder<T, Integer> queryBuilder = dao.queryBuilder();
            Where<T, Integer> where = queryBuilder.where();
            for(int i=0;i<names.length;i++){
                where.eq(names[i], values[i]);
                if(i<names.length-1)where.and();
            }
            PreparedQuery<T> preparedQuery = queryBuilder.prepare();
            return dao.queryForFirst(preparedQuery);
        } catch (SQLException e) {
            e("Unable to query!", e);
            return null;
        }
    }

    public boolean exists(String name) {
        return exists(name, -1);
    }

    public boolean exists(String name, int id) {
        List<T> entities = getEntities(name);
        if (entities == null || entities.isEmpty()) return false;
        if (entities.size() > 1) return true;
        return entities.get(0).getId() != id;
    }

    private List<T> getEntities(String name) {
        try {
            QueryBuilder<T, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.where().eq(PnPUtil.PnPStrings.COLUMN_NAME, name);
            PreparedQuery<T> preparedQuery = queryBuilder.prepare();
            return dao.query(preparedQuery);
        } catch (SQLException e) {
            e("Unable to query!", e);
        }
        return null;
    }

    public T getEntity(String name) {
        List<T> entities = getEntities(name);
        if (entities.size() > 0) return entities.get(0);
        return null;
    }

    public abstract void createDefaults();
}

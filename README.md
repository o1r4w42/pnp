This is a sample project for an android app implementing the following features:
- using fragments to implement different screens
- using fragments to dynamically change content
- using ormlite for jpa repository with sqlite
- using dagger2 for dependency injection
- interface for easy logging

Creating database:
- in PnPDBConfig.java there is a class array with all database entities
    and the static main void determines the name of the generated file for mapping
- run the PnPDBConfig.java with the working directory pointing to "...\app\src\main"
    to generate the mapper file, which is located in "...\app\src\main\res\raw"

Dependency injection:
- PnPInjectionModule has a function for creating every injectable service
- PnPInjectionComponent has a function for every injectable client
- PnPActivity creates the implementation of the PnPInjectionComponent interface
- every injectable client must call the corresponding inject function of the PnPInjectionComponent